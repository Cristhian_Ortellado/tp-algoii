#include "entidades.h"
#include <stdio.h>
#include <stdlib.h>

void inicializarCola(AreaCola *cola, int cantTipoSoli)
{
	cola->frente = cola->final = NULL;
	cola->horaSalidaUlt = 0;
	cola->cantTotalSoli = 0;
	cola->cantTotalSoliProc = 0;
	cola->cantSoliProcesadasPorTipo = (int *)calloc(sizeof(int), cantTipoSoli);
	cola->cantSoliPorTipo = (int *)calloc(sizeof(int), cantTipoSoli);
}

Nodo *crearNodo(Solicitud *soli)
{
	Nodo *nuevoNodo = (Nodo *)malloc(sizeof(Solicitud));
	nuevoNodo->solicitud = soli;
	nuevoNodo->sig = NULL;
	nuevoNodo->atras = NULL;
	return nuevoNodo;
}

int colaVacia(AreaCola *cola)
{
	return cola->frente == NULL;
}

void insertar(AreaCola *cola, Solicitud *soli)
{
	// if (soli == NULL)
	// 	printf("SOLI NULL\n");
	Nodo *nuevoNodo = crearNodo(soli);
	if (colaVacia(cola))
		cola->frente = cola->final = nuevoNodo;
	else
	{
		Nodo *aux = cola->final;
		while (aux != NULL && aux->solicitud->hora_entrada_area > nuevoNodo->solicitud->hora_entrada_area)
		{
			aux = aux->atras;
		}
		if (aux == cola->final)
		{
			cola->final->sig = nuevoNodo;
			nuevoNodo->atras = cola->final;
			cola->final = nuevoNodo;
		}
		else if (aux == NULL)
		{
			nuevoNodo->sig = cola->frente;
			cola->frente->atras = nuevoNodo;
			cola->frente = nuevoNodo;
		}
		else
		{
			nuevoNodo->sig = aux->sig;
			aux->sig->atras = nuevoNodo;
			aux->sig = nuevoNodo;
			nuevoNodo->atras = aux;
		}
	}
}

void desencolar(AreaCola *cola)
{
	if (!colaVacia(cola))
	{
		Nodo *nodoAEliminar = cola->frente;
		cola->frente = cola->frente->sig;
		if (!colaVacia(cola))
			cola->frente->atras = NULL;
		free(nodoAEliminar);
	}
}

void imprimirArea(AreaCola *cola)
{
	if (!colaVacia(cola))
	{
		Nodo *aux = cola->frente;
		for (; aux != cola->final; aux = aux->sig)
			printf("%d -->", aux->solicitud->nro_soli);
		printf("%d\n", aux->solicitud->nro_soli);
	}
	else
		printf("Area vacia\n");
}
