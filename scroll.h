void seleccionarID(int x, int y, int posIni, int posFin, char *str, TipoSolicitud *tiposDeSolicitud);

void seleccionarID(int x, int y, int posIni, int posFin, char *str, TipoSolicitud *tiposDeSolicitud)
{
	int cuadro = 10;
	char temp[9];
	if (y >= 158 && y <= 200)
		cuadro = 0;
	else if (y >= 202 && y <= 247)
		cuadro = 1;
	else if (y >= 249 && y <= 295)
		cuadro = 2;
	else if (y >= 297 && y <= 342)
		cuadro = 3;
	else if (y >= 344 && y <= 388)
		cuadro = 4;
	else if (y >= 390 && y <= 435)
		cuadro = 5;
	else if (y >= 437 && y <= 483)
		cuadro = 6;
	else if (y >= 485 && y <= 530)
		cuadro = 7;
	else if (y >= 532 && y <= 577)
		cuadro = 8;
	else if (y >= 579 && y <= 625)
		cuadro = 9;
	if (posIni + cuadro < posFin)
	{
		sprintf(temp, "%d", tiposDeSolicitud[posIni + cuadro].id_tipo_soli);
		strcpy(str, temp);
	}
}