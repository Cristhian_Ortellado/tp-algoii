#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "simulacion.h"
#include "scroll.h"

#define actualizacionSimulacion 300

typedef struct
{
	char num[10];
} DECIMALES;

typedef struct
{
	char area[2];
} ETAPAS;

//funciones
void inicializarAllegro();
void graficaSimulacion();
void imprimirNroSoli(int area, int x, int y);
void extraerRegistro(char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[]);
void escribirParametroGeneral(ALLEGRO_FONT *fontAreas, char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[]);
void iniciarTipoSoliModificados();

int guardarDatos(ALLEGRO_FONT *fontAreas, int esNuevo, char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[]);
int graficarMenu();
int graficarParGen();
int graficarParEsp();
int graficarSimulacion();
int graficarInforme();
int graficarParGenAM(int tipoInicio);
int DetectarCuadro(int x, int y);
int comprobarDatos(char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[], int esNuevo);
int comprobarExistencia(char textSelectId[], int cantidadExistente);

void graficarError(int opcion);
void graficarTablaParGen(int ini, int fin, char *textSelectId);
void graficarSeleccionadoMenu(int seleccionado);
void graficarSeleccionadoEspecificos(int seleccionado);
void graficarSeleccionadoGenerales(int seleccionado);
void graficarSeleccionadosInforme(int seleccionado);
void graficarSeleccionadoAMGen(int seleccionado);
void datosToString(DECIMALES *text, double numero);

int comprobarParEsp(char *codigoSim, char *duracionSim, char *cantSoli, char *intervaloAct);
void manipularEntrada(ALLEGRO_EVENT evento, char str[], int tipo, int *longitud);

//manejo de allegro
ALLEGRO_DISPLAY *ventana = NULL;
ALLEGRO_EVENT_QUEUE *eventos = NULL;
ALLEGRO_PATH *path = NULL;
ALLEGRO_MOUSE_STATE mouseActual;
ALLEGRO_EVENT eventoActual;
ALLEGRO_SAMPLE *sonidoError = NULL;
ALLEGRO_SAMPLE *sonidoAdvertencia = NULL;

//longitudes para text boxes
int longitudesGenerales[] = {16, 1, 8, 8};
int longitudesEspecificos[] = {4, 5, 5, 5};

//variables de control
int dibujar;
int cambioCursor;

//id del text box
int idModificar = 0;

//Array dinamico que guarda un 1 si se va a modificar su prob a la hora de ajustar las probabilidades
char *tipoSolicitudesModif;
//Guarda el valor de la suma de las prob modificadas
double sumaProbModif;
//Contador de prob Modificadas
int cantProbModif;

int main()
{
	inicializarAllegro();
	inicializarParametrosGenerales();
	tipoSolicitudesModif = (char *)malloc(sizeof(char) * cantTipoSoli);
	iniciarTipoSoliModificados();
	int exit = 1;

	while (exit != -1)
	{
		al_flush_event_queue(eventos);
		al_set_system_mouse_cursor(ventana, 1);
		dibujar = 1;
		cambioCursor = 0;
		//Pantalla Menu
		if (exit == 1)
			exit = graficarMenu();
		//Pantalla de parametros especificos
		else if (exit == 2)
			exit = graficarParEsp();
		//Pantalla de parametros generales
		else if (exit == 3)
			exit = graficarParGen();
		//Pantalla de simulacion
		else if (exit == 4)
		{
			//inicializar estructuras y datos a utilizar en la simulacion
			inicializarParametrosSimulacion();
			exit = graficarSimulacion();
		}
		//pantalla de informe
		else if (exit == 5)
			exit = graficarInforme();
		// pantalla de modificar
		else if (exit == 6)
			exit = graficarParGenAM(1);
		//pantalla agregar
		else if (exit == 7)
			exit = graficarParGenAM(2);
	}

	//cerrar allegro
	al_destroy_sample(sonidoAdvertencia);
	al_destroy_sample(sonidoError);
	al_destroy_path(path);
	al_destroy_event_queue(eventos);
	al_destroy_display(ventana);

	return 0;
}

void inicializarAllegro()
{
	//iniciar allegro y pantalla
	al_init();
	al_set_new_window_title("GESTOR DE DOCUMENTOS");

	//generar evento para detectar posicion de la ventana
	al_set_new_display_flags(ALLEGRO_GENERATE_EXPOSE_EVENTS);
	ventana = al_create_display(1280, 720);
	eventos = al_create_event_queue();

	//se abre la carpeta Allegro en path
	path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_append_path_component(path, "allegro");

	//inicializa los sonidos de errores  y advertencias
	al_install_audio();
	al_init_acodec_addon();
	al_reserve_samples(5);

	sonidoError = al_load_sample("allegro//sonidos//error.wav");
	sonidoAdvertencia = al_load_sample("allegro//sonidos//advertencia.wav");

	//inicializar funciones
	al_init_font_addon();
	al_init_image_addon();
	al_init_ttf_addon();

	//icono
	al_set_path_filename(path, "Icono.png");
	ALLEGRO_BITMAP *icono = al_load_bitmap(al_path_cstr(path, '/'));
	al_set_display_icon(ventana, icono);

	//preparar teclado
	al_install_keyboard();
	al_register_event_source(eventos, al_get_keyboard_event_source());

	//preparar mouse
	al_install_mouse();
	al_register_event_source(eventos, al_get_mouse_event_source());

	//preparar pantalla
	al_register_event_source(eventos, al_get_display_event_source(ventana));
}

void graficarError(int opcion)
{
	ALLEGRO_BITMAP *error = NULL;
	dibujar = 1;

	//Bandera para determinar el tipo de error
	int tipoError = 0;
	//restaurar estado del cursor
	al_set_system_mouse_cursor(ventana, 1);
	cambioCursor = 0;

	switch (opcion)
	{
	case 1:
		al_set_path_filename(path, "error1.png");
		break;
	case 2:
		al_set_path_filename(path, "error2.png");
		break;
	case 3:
		al_set_path_filename(path, "error3.png");
		break;
	case 4:
		al_set_path_filename(path, "error4.png");
		break;
	case 5:
		tipoError = 1;
		al_set_path_filename(path, "error5.png");
		break;
	}
	if (tipoError == 0)
		al_play_sample(sonidoError, 1.0, 0, 1.3, ALLEGRO_PLAYMODE_ONCE, NULL);

	error = al_load_bitmap(al_path_cstr(path, '/'));
	al_draw_bitmap(error, 490, 250, 0);
	al_flip_display();

	//la opcion 5 se maneja en la funcion de simular
	if (opcion != 5)
	{
		while (1)
		{
			al_wait_for_event(eventos, &eventoActual);
			//minimizar
			if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
			{
				al_draw_bitmap(error, 490, 250, 0);
				al_flip_display();
			}
			else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;

				if (mouseActual.x >= 596 && mouseActual.x <= 683 && mouseActual.y >= 400 && mouseActual.y <= 425)
				{
					al_set_system_mouse_cursor(ventana, 17);
					cambioCursor = 1;
				}
				else if (cambioCursor)
				{
					al_set_system_mouse_cursor(ventana, 1);
					cambioCursor = 0;
				}
			}
			else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				if (mouseActual.x >= 596 && mouseActual.x <= 683 && mouseActual.y >= 400 && mouseActual.y <= 425)
					break;
			}
			else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
			{
				//espacio, tab y enter
				if (eventoActual.keyboard.keycode == 67 || eventoActual.keyboard.keycode == 59 || eventoActual.keyboard.keycode == 75)
					break;
			}
		}
	}

	al_set_system_mouse_cursor(ventana, 1);
	al_destroy_bitmap(error);
}

int graficarMenu()
{
	int retorno = 0, seleccionado = 0;

	al_set_path_filename(path, "Menu.png");
	ALLEGRO_BITMAP *pantMenu = al_load_bitmap(al_path_cstr(path, '/'));

	while (1)
	{
		if (dibujar)
		{
			al_draw_bitmap(pantMenu, 0, 0, 0);
			dibujar = 0;
			graficarSeleccionadoMenu(seleccionado);
			al_flip_display();
		}

		//espera que el usuario haga algo
		al_wait_for_event(eventos, &eventoActual);
		if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			retorno = -1;
			break;
		}
		//si se minimiza
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
		{
			dibujar = 1;
		}
		//si se saca la ventana de la pantalla
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
		{
			dibujar = 1;
		}
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;
			if (mouseActual.x >= 512 && mouseActual.x <= 768 && mouseActual.y >= 260 && mouseActual.y <= 342)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 511 && mouseActual.x <= 764 && mouseActual.y >= 393 && mouseActual.y <= 473)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 537 && mouseActual.x <= 737 && mouseActual.y >= 564 && mouseActual.y <= 636)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 1);
				cambioCursor = 0;
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			if (eventoActual.mouse.button == 1)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				if (mouseActual.x >= 512 && mouseActual.x <= 768 && mouseActual.y >= 260 && mouseActual.y <= 342)
				{
					//existen solicitudes
					if (cantTipoSoli)
					{
						retorno = 2;
						break;
					}
					graficarError(1);
				}
				else if (mouseActual.x >= 511 && mouseActual.x <= 764 && mouseActual.y >= 393 && mouseActual.y <= 473)
				{
					retorno = 3;
					break;
				}
				else if (mouseActual.x >= 537 && mouseActual.x <= 737 && mouseActual.y >= 564 && mouseActual.y <= 636)
				{
					retorno = -1;
					break;
				}
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			//tab
			if (eventoActual.keyboard.keycode == 64)
			{
				//shift+tab
				if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					if (seleccionado <= 1)
						seleccionado = 4;
					seleccionado--;
					dibujar = 1;
				}
				//tab
				else
				{
					seleccionado++;
					if (seleccionado == 4)
						seleccionado = 1;
					dibujar = 1;
				}
			}
			//flecha arriba
			else if (eventoActual.keyboard.keycode == 84)
			{
				if (seleccionado <= 1)
					seleccionado = 4;
				seleccionado--;
				dibujar = 1;
			}
			//flecha abajo
			else if (eventoActual.keyboard.keycode == 85)
			{
				seleccionado++;
				if (seleccionado == 4)
					seleccionado = 1;
				dibujar = 1;
			}
			//escape
			else if (eventoActual.keyboard.keycode == 59)
			{
				if (seleccionado)
					dibujar = 1;
				seleccionado = 0;
			}
			else if (eventoActual.keyboard.keycode == 67 && seleccionado)
			{
				switch (seleccionado)
				{
				case 1:
					//existen solicitudes
					if (cantTipoSoli)
					{
						retorno = 2;
						break;
					}
					graficarError(1);
					break;
				case 2:
					retorno = 3;
					break;
				case 3:
					retorno = -1;
					break;
				}
				if (retorno != 0)
					break;
			}
		}
	}

	al_destroy_bitmap(pantMenu);
	return retorno;
}

void graficarSeleccionadoMenu(int seleccionado)
{
	al_set_path_filename(path, "Marcador.png");
	ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

	switch (seleccionado)
	{
	case 1:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 518.2, 347, 245, 4, 0);
		break;
	case 2:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 518.2, 483, 245, 4, 0);
		break;
	case 3:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 545, 646, 190, 4, 0);
		break;
	}
	al_destroy_bitmap(senhalador);
}

int graficarParEsp()
{
	int retorno = 0;
	al_set_path_filename(path, "Parametrosespecificos.png");
	ALLEGRO_BITMAP *pantParEspec = al_load_bitmap(al_path_cstr(path, '/'));

	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontDatos = al_load_font(al_path_cstr(path, '/'), 30, 0);

	char codigoSim[5] = {""};
	char duracionSim[6] = {""};
	char cantSoli[6] = {""};
	char intervaloAct[6] = {""};
	int seleccionado = 0;

	while (1)
	{
		if (dibujar == 1)
		{
			dibujar = 0;
			al_draw_bitmap(pantParEspec, 0, 0, 0);
			al_draw_text(fontDatos, al_map_rgb(0, 0, 0), 795, 195, 0, codigoSim);
			al_draw_text(fontDatos, al_map_rgb(0, 0, 0), 795, 265, 0, duracionSim);
			al_draw_text(fontDatos, al_map_rgb(0, 0, 0), 795, 330, 0, cantSoli);
			al_draw_text(fontDatos, al_map_rgb(0, 0, 0), 795, 395, 0, intervaloAct);
			graficarSeleccionadoEspecificos(seleccionado);
			al_flip_display();
		}

		//limpiar el evento actual para no repetir la ultima accion
		memset(&eventoActual, 0, sizeof(ALLEGRO_EVENT));
		al_wait_for_event_timed(eventos, &eventoActual, 0.5);
		if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			retorno = -1;
			break;
		}
		//si se minimiza
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
			dibujar = 1;
		//si se saca la ventana de la pantalla
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
			dibujar = 1;
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;
			if (mouseActual.x >= 362 && mouseActual.x <= 514 && mouseActual.y >= 580 && mouseActual.y <= 618)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 784 && mouseActual.x <= 935 && mouseActual.y >= 580 && mouseActual.y <= 618)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 964 && mouseActual.x <= 1195 && mouseActual.y >= 59 && mouseActual.y <= 95)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 189 && mouseActual.y <= 226)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 255 && mouseActual.y <= 293)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 320 && mouseActual.y <= 359)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 386 && mouseActual.y <= 425)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 1);
				cambioCursor = 0;
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			if (eventoActual.mouse.button == 1)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				if (mouseActual.x >= 362 && mouseActual.x <= 514 && mouseActual.y >= 580 && mouseActual.y <= 618)
				{
					if (comprobarParEsp(codigoSim, duracionSim, cantSoli, intervaloAct))
					{
						escribirUltimoPar(params);
						retorno = 4;
						break;
					}
					else
						graficarError(4);
				}
				else if (mouseActual.x >= 964 && mouseActual.x <= 1195 && mouseActual.y >= 59 && mouseActual.y <= 95)
				{
					if (!leerUltimoPar(codigoSim, duracionSim, cantSoli, intervaloAct))
						graficarError(2);
					else
						dibujar = 1;
				}
				else if (mouseActual.x >= 784 && mouseActual.x <= 935 && mouseActual.y >= 580 && mouseActual.y <= 618)
				{
					retorno = 1;
					break;
				}
				else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 189 && mouseActual.y <= 226)
				{
					if (seleccionado != 1)
						dibujar = 1;
					seleccionado = 1;
				}
				else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 255 && mouseActual.y <= 293)
				{
					if (seleccionado != 2)
						dibujar = 1;
					seleccionado = 2;
				}
				else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 320 && mouseActual.y <= 359)
				{
					if (seleccionado != 3)
						dibujar = 1;
					seleccionado = 3;
				}
				else if (mouseActual.x >= 785 && mouseActual.x <= 938 && mouseActual.y >= 386 && mouseActual.y <= 425)
				{
					if (seleccionado != 4)
						dibujar = 1;
					seleccionado = 4;
				}
				else
				{
					if (seleccionado)
						dibujar = 1;
					seleccionado = 0;
				}
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			//tab
			if (eventoActual.keyboard.keycode == 64)
			{
				//shift+tab
				if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					if (seleccionado <= 1)
						seleccionado = 5;
					seleccionado--;
					dibujar = 1;
				}
				//tab
				else
				{
					seleccionado++;
					if (seleccionado >= 5)
						seleccionado = 1;
					dibujar = 1;
				}
			}
			//flecha abajo
			else if (eventoActual.keyboard.keycode == 85)
			{
				seleccionado++;
				if (seleccionado >= 5)
					seleccionado = 1;
				dibujar = 1;
			}
			//flecha arriba
			else if (eventoActual.keyboard.keycode == 84)
			{
				if (seleccionado <= 1)
					seleccionado = 5;
				seleccionado--;
				dibujar = 1;
			}
			//flecha izquierda y derecha
			else if (eventoActual.keyboard.keycode == 82 || eventoActual.keyboard.keycode == 83)
			{
				if (seleccionado == 5 || seleccionado == 6)
				{
					if (seleccionado == 5)
						seleccionado = 6;
					else
						seleccionado = 5;
					dibujar = 1;
				}
			}
			//escape
			else if (eventoActual.keyboard.keycode == 59)
			{
				if (seleccionado == 5 || seleccionado == 6)
					seleccionado = 1;
				else
					seleccionado = 5;
				dibujar = 1;
			}
			//enter
			else if (eventoActual.keyboard.keycode == 67)
			{
				if (seleccionado == 5)
				{
					if (comprobarParEsp(codigoSim, duracionSim, cantSoli, intervaloAct))
					{
						escribirUltimoPar(params);
						retorno = 4;
						break;
					}
					else
						graficarError(4);
				}
				else if (seleccionado == 6)
				{
					retorno = 1;
					break;
				}
				else if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					if (!leerUltimoPar(codigoSim, duracionSim, cantSoli, intervaloAct))
						graficarError(2);
					else
						dibujar = 1;
				}
			}
			if (seleccionado != 0 && seleccionado != 5 && seleccionado != 6)
			{
				if (seleccionado == 1)
					manipularEntrada(eventoActual, codigoSim, 1, longitudesEspecificos);
				else if (seleccionado == 2)
					manipularEntrada(eventoActual, duracionSim, 3, longitudesEspecificos);
				else if (seleccionado == 3)
					manipularEntrada(eventoActual, cantSoli, 4, longitudesEspecificos);
				else if (seleccionado == 4)
					manipularEntrada(eventoActual, intervaloAct, 3, longitudesEspecificos);
			}
		}
	}

	al_destroy_bitmap(pantParEspec);
	al_destroy_font(fontDatos);
	return retorno;
}

void graficarSeleccionadoEspecificos(int seleccionado)
{
	al_set_path_filename(path, "Marcador.png");
	ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

	switch (seleccionado)
	{
	case 1:
		al_draw_bitmap(senhalador, 793, 230, 0);
		break;
	case 2:
		al_draw_bitmap(senhalador, 793, 301, 0);
		break;
	case 3:
		al_draw_bitmap(senhalador, 793, 366, 0);
		break;
	case 4:
		al_draw_bitmap(senhalador, 793, 430, 0);
		break;
	case 5:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 365, 622, 148, 4, 0);
		break;
	case 6:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 785, 622, 148, 4, 0);
		break;
	}
	al_destroy_bitmap(senhalador);
}

int graficarParGen()
{
	int retorno = 0, seleccionado = 0;

	char textSelectId[9];
	int posIni = 0;
	int posFin;
	int selectId;
	strcpy(textSelectId, "");

	al_set_path_filename(path, "Parametrosgenerales.png");
	ALLEGRO_BITMAP *pantTabla = al_load_bitmap(al_path_cstr(path, '/'));

	if (mouseActual.x >= 101 && mouseActual.x <= 1180 && mouseActual.y >= 157 && mouseActual.y <= 624)
	{
		al_set_system_mouse_cursor(ventana, 17);
		cambioCursor = 1;
	}

	if (cantTipoSoli < 10)
		posFin = cantTipoSoli;
	else
		posFin = 10;

	while (1)
	{
		if (dibujar)
		{
			dibujar = 0;
			al_draw_bitmap(pantTabla, 0, 0, 0);
			graficarTablaParGen(posIni, posFin, textSelectId);
			graficarSeleccionadoGenerales(seleccionado);
			al_flip_display();
		}

		//espera que el usuario haga algo
		al_wait_for_event(eventos, &eventoActual);
		if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			retorno = -1;
			break;
		}
		//si se minimiza
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
			dibujar = 1;
		//si se saca la ventana de la pantalla
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
			dibujar = 1;
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;
			if (mouseActual.x >= 998 && mouseActual.x <= 1182 && mouseActual.y >= 654 && mouseActual.y <= 695)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 787 && mouseActual.x <= 972 && mouseActual.y >= 654 && mouseActual.y <= 695)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 578 && mouseActual.x <= 762 && mouseActual.y >= 654 && mouseActual.y <= 695)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 364 && mouseActual.x <= 547 && mouseActual.y >= 654 && mouseActual.y <= 695)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 144 && mouseActual.x <= 266 && mouseActual.y >= 654 && mouseActual.y <= 695)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 101 && mouseActual.x <= 1180 && mouseActual.y >= 157 && mouseActual.y <= 624)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 1);
				cambioCursor = 0;
			}
			if (eventoActual.mouse.z > mouseActual.z && posIni != 0)
			{
				posIni--;
				posFin--;
				dibujar = 1;
			}
			else if (eventoActual.mouse.z < mouseActual.z && posFin < cantTipoSoli)
			{
				posIni++;
				posFin++;
				dibujar = 1;
			}
			mouseActual.z = eventoActual.mouse.z;
		}
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			if (eventoActual.mouse.button == 1)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				//atras
				if (mouseActual.x >= 998 && mouseActual.x <= 1182 && mouseActual.y >= 654 && mouseActual.y <= 695)
				{
					retorno = 1;
					iniciarTipoSoliModificados();
					break;
				}
				//agregar
				else if (mouseActual.x >= 368 && mouseActual.x <= 552 && mouseActual.y >= 654 && mouseActual.y <= 695)
				{
					idModificar = cantTipoSoli + 1;
					retorno = 7;
					break;
				}
				//borrar
				else if (mouseActual.x >= 790 && mouseActual.x <= 974 && mouseActual.y >= 654 && mouseActual.y <= 695)
				{
					if (comprobarExistencia(textSelectId, cantTipoSoli) && cantTipoSoli > 1)
					{
						selectId = atoi(textSelectId);
						double valor = tiposDeSolicitud[selectId - 1].prob_soli;

						//Eliminamos el tipo seleccionado
						eliminarTipoSolicitud(&tiposDeSolicitud, &tramites, selectId, &cantTipoSoli);

						tipoSolicitudesModif = (char *)realloc(tipoSolicitudesModif, cantTipoSoli);
						//Ajustamos las probabilidades
						ajustarProbabilidad(tiposDeSolicitud, cantTipoSoli, valor, 1 - valor, tipoSolicitudesModif);

						//Escribimos en el archivo
						escribirTipoDeSolicitudes(tiposDeSolicitud, cantTipoSoli);
						escribirTramites(tramites, cantTipoSoli);

						if (cantTipoSoli < 10)
						{
							posIni = 0;
							posFin = cantTipoSoli;
						}
						else if (posFin > cantTipoSoli)
						{
							posIni--;
							posFin--;
						}
						strcpy(textSelectId, "");
						dibujar = 1;
					}
					else
						graficarError(3);
				}
				//modificar
				else if (mouseActual.x >= 582 && mouseActual.x <= 766 && mouseActual.y >= 654 && mouseActual.y <= 695)
				{
					if (comprobarExistencia(textSelectId, cantTipoSoli))
					{
						idModificar = atoi(textSelectId);
						retorno = 6;
						break;
					}
					else
						graficarError(3);
				}
				//ID
				else if (mouseActual.x >= 144 && mouseActual.x <= 266 && mouseActual.y >= 654 && mouseActual.y <= 695)
				{
					if (seleccionado != 1)
						dibujar = 1;
					seleccionado = 1;
				}
				//se toco la tabla
				else if (mouseActual.x >= 101 && mouseActual.x <= 1180 && mouseActual.y >= 157 && mouseActual.y <= 624)
				{
					if (cantTipoSoli)
					{
						seleccionarID(mouseActual.x, mouseActual.y, posIni, posFin, textSelectId, tiposDeSolicitud);
						dibujar = 1;
					}
				}
				//desmarcar
				else
				{
					if (seleccionado)
						dibujar = 1;
					seleccionado = 0;
				}
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			//tab
			if (eventoActual.keyboard.keycode == 64)
			{
				//shift+tab
				if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					if (seleccionado <= 1)
						seleccionado = 6;
					seleccionado--;
					dibujar = 1;
				}
				//tab
				else
				{
					seleccionado++;
					if (seleccionado == 6)
						seleccionado = 1;
					dibujar = 1;
				}
			}
			//flecha izquierda
			else if (eventoActual.keyboard.keycode == 82)
			{
				if (seleccionado <= 1)
					seleccionado = 6;
				seleccionado--;
				dibujar = 1;
			}
			//flecha derecha
			else if (eventoActual.keyboard.keycode == 83)
			{
				seleccionado++;
				if (seleccionado == 6)
					seleccionado = 1;
				dibujar = 1;
			}
			//escape
			else if (eventoActual.keyboard.keycode == 59)
			{
				if (seleccionado)
					dibujar = 1;
				seleccionado = 0;
			}
			//flecha arriba
			else if (eventoActual.keyboard.keycode == 84 && posIni != 0)
			{
				posIni--;
				posFin--;
				dibujar = 1;
			}
			// //flecha abajo
			else if (eventoActual.keyboard.keycode == 85 && posFin < cantTipoSoli)
			{
				posIni++;
				posFin++;
				dibujar = 1;
			}
			//enter
			else if (eventoActual.keyboard.keycode == 67 && seleccionado)
			{
				switch (seleccionado)
				{
				case 2: //agregar
					idModificar = cantTipoSoli + 1;
					retorno = 7;
					break;
				case 3: //modificar
					if (comprobarExistencia(textSelectId, cantTipoSoli))
					{
						idModificar = atoi(textSelectId);
						retorno = 6;
						break;
					}
					else
						graficarError(3);
					break;
				case 4: //borrar
					if (comprobarExistencia(textSelectId, cantTipoSoli) && cantTipoSoli > 1)
					{
						selectId = atoi(textSelectId);
						double valor = tiposDeSolicitud[selectId - 1].prob_soli;

						//Eliminamos el tipo seleccionado
						eliminarTipoSolicitud(&tiposDeSolicitud, &tramites, selectId, &cantTipoSoli);

						//Ajustamos las probabilidades
						ajustarProbabilidad(tiposDeSolicitud, cantTipoSoli, valor, 1 - valor, tipoSolicitudesModif);

						//Escribimos en el archivo
						escribirTipoDeSolicitudes(tiposDeSolicitud, cantTipoSoli);
						escribirTramites(tramites, cantTipoSoli);

						if (cantTipoSoli < 10)
							posFin = cantTipoSoli;
						else
							posFin = 10;

						strcpy(textSelectId, "");
						dibujar = 1;
					}
					else
						graficarError(3);
					break;
				case 5: //atras
					retorno = 1;
					break;
				}
				if (retorno != 0)
					break;
			}
			if (seleccionado == 1)
				manipularEntrada(eventoActual, textSelectId, 4, longitudesGenerales);
		}
	}

	al_destroy_bitmap(pantTabla);
	return retorno;
}

void graficarTablaParGen(int ini, int fin, char *textSelectId)
{
	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontTabla = al_load_font(al_path_cstr(path, '/'), 30, 0);

	int espacio = 0;
	for (int i = ini; i < fin; i++)
	{
		al_draw_textf(fontTabla, al_map_rgb(0, 0, 0), 181, (espacio * 48) + 168, 1, "%d", tiposDeSolicitud[i].id_tipo_soli);
		al_draw_textf(fontTabla, al_map_rgb(0, 0, 0), 280, (espacio * 48) + 168, 0, "%s", tiposDeSolicitud[i].nom_soli);
		al_draw_textf(fontTabla, al_map_rgb(0, 0, 0), 778, (espacio * 48) + 168, 0, "%.5f", tiposDeSolicitud[i].prob_soli);

		espacio++;
	}
	al_draw_text(fontTabla, al_map_rgb(0, 0, 0), 150, 666, 0, textSelectId);
	al_destroy_font(fontTabla);
}

void graficarSeleccionadoGenerales(int seleccionado)
{
	al_set_path_filename(path, "Marcador.png");
	ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

	switch (seleccionado)
	{
	case 1:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 150, 700, 110, 4, 0);
		break;
	case 2:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 369, 700, 175, 4, 0);
		break;
	case 3:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 583, 700, 175, 4, 0);
		break;
	case 4:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 793.5, 700, 175, 4, 0);
		break;
	case 5:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 1002.8, 700, 175, 4, 0);
		break;
	}
	al_destroy_bitmap(senhalador);
}

int graficarSimulacion()
{
	int retorno = 0;

	int adelantar = 0, simular = 1, contFPS = 0;
	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontSkip = al_load_font(al_path_cstr(path, '/'), 30, 0);

	ALLEGRO_SAMPLE *campana = al_load_sample("allegro//sonidos//campana.wav");

	while (1)
	{
		if (dibujar)
		{
			graficaSimulacion();
			dibujar = 0;
			//verificar que mouse colocar tras terminar la actualizacion
			al_get_mouse_state(&mouseActual);
			if (mouseActual.x >= 985 && mouseActual.x <= 1138 && mouseActual.y >= 619 && mouseActual.y <= 676)
				al_set_system_mouse_cursor(ventana, 17);
			else
				al_set_system_mouse_cursor(ventana, 1);
			al_flip_display();
			al_flush_event_queue(eventos);
		}

		//espera que el usuario haga algo en un tiempo
		if (al_wait_for_event_timed(eventos, &eventoActual, 0.01))
		{
			if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			{
				al_set_path_filename(path, "Marcador.png");
				ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

				al_play_sample(sonidoAdvertencia, 1.0, 0, 1.45, ALLEGRO_PLAYMODE_ONCE, NULL);

				int seleccionado = 1;
				dibujar = 1;
				while (1)
				{
					if (dibujar)
					{
						graficarError(5);
						if (seleccionado == 1)
							al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 525, 434, 90, 4, 0);
						else
							al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 672, 434, 90, 4, 0);
						dibujar = 0;
						al_flip_display();
					}
					al_wait_for_event(eventos, &eventoActual);
					//si se minimiza
					if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
						dibujar = 1;
					//si se saca la ventana de la pantalla
					else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
						dibujar = 1;
					else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
					{
						mouseActual.x = eventoActual.mouse.x;
						mouseActual.y = eventoActual.mouse.y;
						if (mouseActual.x >= 522 && mouseActual.x <= 617 && mouseActual.y >= 405 && mouseActual.y <= 430)
						{
							al_set_system_mouse_cursor(ventana, 17);
							cambioCursor = 1;
						}
						else if (mouseActual.x >= 670 && mouseActual.x <= 766 && mouseActual.y >= 405 && mouseActual.y <= 430)
						{
							al_set_system_mouse_cursor(ventana, 17);
							cambioCursor = 1;
						}
						else if (cambioCursor)
						{
							al_set_system_mouse_cursor(ventana, 1);
							cambioCursor = 0;
						}
					}
					else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
					{
						if (eventoActual.mouse.button == 1)
						{
							mouseActual.x = eventoActual.mouse.x;
							mouseActual.y = eventoActual.mouse.y;

							//se toco aceptar
							if (mouseActual.x >= 522 && mouseActual.x <= 617 && mouseActual.y >= 405 && mouseActual.y <= 430)
							{
								retorno = -1;
								break;
							}
							//cancelar
							else if (mouseActual.x >= 670 && mouseActual.x <= 766 && mouseActual.y >= 405 && mouseActual.y <= 430)
								break;
						}
					}
					else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
					{
						//flecha izquierda, derecha y tab
						if (eventoActual.keyboard.keycode == 82 || eventoActual.keyboard.keycode == 83 || eventoActual.keyboard.keycode == 64)
						{
							if (seleccionado == 1)
								seleccionado = 2;
							else
								seleccionado = 1;
							dibujar = 1;
						}
						//enter
						else if (eventoActual.keyboard.keycode == 67)
						{
							if (seleccionado == 1)
							{
								retorno = -1;
								break;
							}
							else
								break;
						}
					}
				}
				al_destroy_bitmap(senhalador);
				dibujar = 1;
				if (retorno == -1)
					break;
			}
			//si se minimiza
			else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
				dibujar = 1;
			//si se saca la ventana de la pantalla
			else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
				dibujar = 1;
			else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				if (mouseActual.x >= 985 && mouseActual.x <= 1138 && mouseActual.y >= 619 && mouseActual.y <= 676)
					al_set_system_mouse_cursor(ventana, 17);
				else
					al_set_system_mouse_cursor(ventana, 1);
			}
			else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
			{
				if (eventoActual.mouse.button == 1)
				{
					mouseActual.x = eventoActual.mouse.x;
					mouseActual.y = eventoActual.mouse.y;

					//se toco el boton de adelantar, se obtiene los nuevos datos y se grafica
					if (mouseActual.x >= 985 && mouseActual.x <= 1138 && mouseActual.y >= 619 && mouseActual.y <= 676)
						adelantar = 1;
				}
			}
			else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
			{
				//espacio
				if (eventoActual.keyboard.keycode == 75)
					adelantar = 1;
			}
		}
		contFPS++;

		//adelantar manual
		if (adelantar)
		{
			al_play_sample(campana, 1.0, 0, 1.45, ALLEGRO_PLAYMODE_ONCE, NULL);
			adelantar = 0;
			al_set_system_mouse_cursor(ventana, 15);
			dibujar = 1;
			contFPS = 0;
			simular = obtenerInfoSimulacion();

			//simular pausa
			if (simular)
			{
				al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 986, 550, 0, "Adelantando");
				al_flip_display();
				al_rest(0.2);
				al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1046, 570, 0, ".");
				al_flip_display();
				al_rest(0.2);
				al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1061, 570, 0, ".");
				al_flip_display();
				al_rest(0.2);
				al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1076, 570, 0, ".");
				al_flip_display();
				al_rest(0.2);
			}
		}

		//se actualiza automaticamente
		if (contFPS == actualizacionSimulacion)
		{
			al_play_sample(campana, 1.0, 0, 1.45, ALLEGRO_PLAYMODE_ONCE, NULL);
			contFPS = 0;
			al_set_system_mouse_cursor(ventana, 15);
			simular = obtenerInfoSimulacion();

			//simular pausa
			al_rest(0.3);
			dibujar = 1;
		}

		//fin de simulacion
		if (!simular)
		{
			graficaSimulacion();
			al_flip_display();
			al_set_system_mouse_cursor(ventana, 15);

			//simular pausa al terminar la simulacion
			contFPS = 0;
			while (contFPS != 400)
			{
				if (contFPS == 80)
				{
					al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 991, 550, 0, "Finalizando");
					al_flip_display();
					al_rest(0.2);
					al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1046, 570, 0, ".");
					al_flip_display();
					al_rest(0.2);
					al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1061, 570, 0, ".");
					al_flip_display();
					al_rest(0.2);
					al_draw_text(fontSkip, al_map_rgb(255, 255, 255), 1076, 570, 0, ".");
					al_flip_display();
					al_rest(0.2);
					contFPS = 299;
				}
				contFPS++;
			}
			al_set_system_mouse_cursor(ventana, 1);
			retorno = 5;
			break;
		}
	}

	//liberar memoria
	al_destroy_sample(campana);
	al_destroy_font(fontSkip);
	return retorno;
}

void graficaSimulacion()
{
	//se carga pantalla de simulacion
	al_set_path_filename(path, "Simulacion.png");
	ALLEGRO_BITMAP *pantSimulacion = al_load_bitmap(al_path_cstr(path, '/'));

	//se carga fuente
	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontAreas = al_load_font(al_path_cstr(path, '/'), 36, 0);

	//se grafica pantalla de simulacion
	al_draw_bitmap(pantSimulacion, 0, 0, 0);

	//se grafica el id de la actual solicitud en cada area
	imprimirNroSoli(0, 253, 94);
	imprimirNroSoli(1, 253, 220);
	imprimirNroSoli(2, 253, 344);
	imprimirNroSoli(3, 253, 466);
	imprimirNroSoli(4, 253, 599);

	//Dibujamos las solicitudes que llegaron en el intervalo
	al_draw_textf(fontAreas, al_map_rgb(0, 0, 0), 1058, 324, ALLEGRO_ALIGN_CENTRE, "%d", cantSoliNuevas);

	//Dibujamos las solicitudes que fueron procesadas en ese intervalo
	al_draw_textf(fontAreas, al_map_rgb(0, 0, 0), 1058, 480, ALLEGRO_ALIGN_CENTRE, "%d", cantSoliSalientes);

	//se grafica el reloj
	al_draw_text(fontAreas, al_map_rgb(255, 255, 255), 993, 95, 0, reloj);

	//liberar memoria
	al_destroy_font(fontAreas);
	al_destroy_bitmap(pantSimulacion);
}

void imprimirNroSoli(int area, int x, int y)
{
	if (!colaVacia(&areasColas[area]))
	{
		//se obtiene el id actual de cada area y la cant. de solicitudes en el area
		int nroSoli = areasColas[area].frente->solicitud->nro_soli;
		int cantSoliArea = areasColas[area].cantTotalSoli;
		float porcentaje = (float)cantSoliArea / params.cant_soli;

		//se carga la barra para llenar
		al_set_path_filename(path, "Barrasimulacion.png");
		ALLEGRO_BITMAP *barraSimulacion = al_load_bitmap(al_path_cstr(path, '/'));

		//fuentes
		ALLEGRO_FONT *fontNroSoli;
		ALLEGRO_FONT *fontSoliSaliente;
		al_set_path_filename(path, "arial_narrow_7.ttf");

		//determinar tamaño para las fuentes
		if (nroSoli > 9999)
		{
			fontNroSoli = al_load_font(al_path_cstr(path, '/'), 19, 0);
		}
		else
		{
			fontNroSoli = al_load_font(al_path_cstr(path, '/'), 25, 0);
		}

		//determinar tamaño para las fuentes
		if (cantSoliArea > 99999)
		{
			fontSoliSaliente = al_load_font(al_path_cstr(path, '/'), 19, 0);
		}
		else
		{
			fontSoliSaliente = al_load_font(al_path_cstr(path, '/'), 25, 0);
		}

		//se grafica la barra de llenado para cada area individualmente
		if (area == 0)
		{
			al_draw_scaled_bitmap(barraSimulacion, 0, 0, 470, 39, x + 99, y + 18, 470 * porcentaje, 39, 0);
		}
		else if (area == 1)
		{
			al_draw_scaled_bitmap(barraSimulacion, 0, 0, 470, 39, x + 99, y + 18, 470 * porcentaje, 39, 0);
		}
		else if (area == 2)
		{
			al_draw_scaled_bitmap(barraSimulacion, 0, 0, 470, 30, x + 99, y + 19, 470 * porcentaje, 39, 0);
		}
		else if (area == 3)
		{
			al_draw_scaled_bitmap(barraSimulacion, 0, 0, 470, 39, x + 99, y + 23, 470 * porcentaje, 39, 0);
		}
		else if (area == 4)
		{
			al_draw_scaled_bitmap(barraSimulacion, 0, 0, 470, 39, x + 99, y + 18, 470 * porcentaje, 39, 0);
		}

		//se grafican los datos obtenidos
		al_draw_textf(fontNroSoli, al_map_rgb(0, 0, 0), x, y, ALLEGRO_ALIGN_CENTRE, "%d", nroSoli);
		al_draw_textf(fontSoliSaliente, al_map_rgb(0, 0, 0), x + 618, y + 30, ALLEGRO_ALIGN_CENTRE, "%d", cantSoliArea);

		//destruir las fuentes para liberar la memoria afuera de la funcion
		al_destroy_font(fontNroSoli);
		al_destroy_font(fontSoliSaliente);
		al_destroy_bitmap(barraSimulacion);
	}
}

int graficarInforme()
{
	int retorno = 0, seleccionado = 0;

	//carga el sonido
	ALLEGRO_SAMPLE *fondWriter = al_load_sample("allegro//sonidos//fondowriter.wav");
	ALLEGRO_SAMPLE *typeWriter = al_load_sample("allegro//sonidos//typewriter.wav");

	al_set_path_filename(path, "Informe.png");
	ALLEGRO_BITMAP *pantInforme = al_load_bitmap(al_path_cstr(path, '/'));

	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontInforme = al_load_font(al_path_cstr(path, '/'), 30, 0);

	//comando para seleccionar archivo
	char abrirCarpeta[75] = {"explorer.exe /select, "};
	filename[8] = '\\';
	strcat(abrirCarpeta, filename);
	filename[8] = '/';

	//empieza el sonido
	al_play_sample(fondWriter, 1.0, 0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
	al_play_sample(typeWriter, 1.0, 0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

	while (1)
	{
		if (dibujar)
		{
			dibujar = 0;
			al_draw_bitmap(pantInforme, 0, 0, 0);
			for (int i = 0; i < 5; i++)
			{
				al_draw_textf(fontInforme, al_map_rgb(0, 0, 0), 290, (i * 51) + 250, 0, "%d", i + 1);
				al_draw_textf(fontInforme, al_map_rgb(0, 0, 0), 442, (i * 51) + 250, 0, "%d", areasColas[i].cantTotalSoliProc);
				al_draw_textf(fontInforme, al_map_rgb(0, 0, 0), 730, (i * 51) + 250, 0, "%d", areasColas[i].cantTotalSoli);
			}
			al_draw_textf(fontInforme, al_map_rgb(0, 0, 0), 442, 492, 0, "%d", cantTotalProcesadas);
			al_draw_textf(fontInforme, al_map_rgb(0, 0, 0), 730, 492, 0, "%d", cantTotalPendientes);
			graficarSeleccionadosInforme(seleccionado);
			al_flip_display();
		}

		//espera que el usuario haga algo
		al_wait_for_event(eventos, &eventoActual);
		if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			retorno = -1;
			break;
		}
		//si se minimiza
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
			dibujar = 1;
		//si se saca la ventana de la pantalla
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
			dibujar = 1;
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;
			if (mouseActual.x >= 283 && mouseActual.x <= 475 && mouseActual.y >= 576 && mouseActual.y <= 616)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 542 && mouseActual.x <= 733 && mouseActual.y >= 576 && mouseActual.y <= 616)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (mouseActual.x >= 798 && mouseActual.x <= 988 && mouseActual.y >= 576 && mouseActual.y <= 616)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 1);
				cambioCursor = 0;
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			if (eventoActual.mouse.button == 1)
			{
				mouseActual.x = eventoActual.mouse.x;
				mouseActual.y = eventoActual.mouse.y;
				if (mouseActual.x >= 283 && mouseActual.x <= 475 && mouseActual.y >= 576 && mouseActual.y <= 616)
				{
					retorno = 1;
					break;
				}
				else if (mouseActual.x >= 542 && mouseActual.x <= 733 && mouseActual.y >= 576 && mouseActual.y <= 616)
				{
					al_set_system_mouse_cursor(ventana, 15);
					filename[8] = '\\';
					system(filename);
					filename[8] = '/';
					al_set_system_mouse_cursor(ventana, 1);
				}
				else if (mouseActual.x >= 798 && mouseActual.x <= 988 && mouseActual.y >= 576 && mouseActual.y <= 616)
				{
					al_set_system_mouse_cursor(ventana, 15);
					system(abrirCarpeta);
					al_set_system_mouse_cursor(ventana, 1);
				}
			}
		}
		else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			//tab
			if (eventoActual.keyboard.keycode == 64)
			{
				//shift+tab
				if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					if (seleccionado <= 1)
						seleccionado = 4;
					seleccionado--;
					dibujar = 1;
				}
				//tab
				else
				{
					seleccionado++;
					if (seleccionado == 4)
						seleccionado = 1;
					dibujar = 1;
				}
			}
			//flecha izquierda
			else if (eventoActual.keyboard.keycode == 82)
			{
				if (seleccionado <= 1)
					seleccionado = 4;
				seleccionado--;
				dibujar = 1;
			}
			//flecha derecha
			else if (eventoActual.keyboard.keycode == 83)
			{
				seleccionado++;
				if (seleccionado == 4)
					seleccionado = 1;
				dibujar = 1;
			}
			//escape
			else if (eventoActual.keyboard.keycode == 59)
			{
				if (seleccionado)
					dibujar = 1;
				seleccionado = 0;
			}
			else if (eventoActual.keyboard.keycode == 67 && seleccionado)
			{
				switch (seleccionado)
				{
				case 1:
					retorno = 1;
					break;
				case 2:
					al_set_system_mouse_cursor(ventana, 15);
					filename[8] = '\\';
					system(filename);
					filename[8] = '/';
					al_set_system_mouse_cursor(ventana, 1);
					break;
				case 3:
					al_set_system_mouse_cursor(ventana, 15);
					system(abrirCarpeta);
					al_set_system_mouse_cursor(ventana, 1);
					break;
				}
				if (retorno != 0)
					break;
			}
		}
	}

	//se libera la memoria
	al_destroy_sample(fondWriter);
	al_destroy_sample(typeWriter);
	al_destroy_font(fontInforme);
	al_destroy_bitmap(pantInforme);

	return retorno;
}

void graficarSeleccionadosInforme(int seleccionado)
{
	al_set_path_filename(path, "Marcador.png");
	ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

	switch (seleccionado)
	{
	case 1:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 290, 623, 175, 4, 0);
		break;
	case 2:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 550, 623, 175, 4, 0);
		break;
	case 3:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 806, 623, 175, 4, 0);
		break;
	}
	al_destroy_bitmap(senhalador);
}

void manipularEntrada(ALLEGRO_EVENT evento, char str[], int tipo, int *longitud)
{
	int len = strlen(str);
	//borrar
	if (evento.keyboard.keycode == ALLEGRO_KEY_BACKSPACE && len != 0)
	{
		str[len - 1] = '\0';
		dibujar = 1;
	}
	//si aun no se lleno el vector
	else if (len < longitud[tipo - 1])
	{
		//alfanumerico
		if (tipo == 1)
		{
			char temp[] = {evento.keyboard.unichar, '\0'};
			if (evento.keyboard.unichar == ' ')
			{
				strcat(str, temp);
				dibujar = 1;
			}
			else if (evento.keyboard.unichar >= '0' && evento.keyboard.unichar <= '9')
			{
				strcat(str, temp);
				dibujar = 1;
			}
			else if (evento.keyboard.unichar >= 'A' && evento.keyboard.unichar <= 'Z')
			{
				strcat(str, temp);
				dibujar = 1;
			}
			else if (evento.keyboard.unichar >= 'a' && evento.keyboard.unichar <= 'z')
			{
				strcat(str, temp);
				dibujar = 1;
			}
		}
		//areas (0-5)
		else if (tipo == 2)
		{
			char temp[] = {evento.keyboard.unichar, '\0'};
			if (evento.keyboard.unichar >= '0' && evento.keyboard.unichar <= '5')
			{
				strcat(str, temp);
				dibujar = 1;
			}
		}
		//numeros con coma
		else if (tipo == 3)
		{
			char temp[] = {evento.keyboard.unichar, '\0'};
			if (evento.keyboard.unichar >= '0' && evento.keyboard.unichar <= '9')
			{
				strcat(str, temp);
				dibujar = 1;
			}
			else if (evento.keyboard.unichar == '.')
			{
				int i = 0;
				int ban = 0;
				while (str[i] != '\0')
				{
					if (str[i] == '.')
					{
						ban = 1;
						break;
					}
					i++;
				}
				if (str[0] != '\0' && ban == 0)
				{
					strcat(str, temp);
					dibujar = 1;
				}
			}
		}
		//numeros enteros
		else if (tipo == 4)
		{
			char temp[] = {evento.keyboard.unichar, '\0'};
			if (evento.keyboard.unichar >= '0' && evento.keyboard.unichar <= '9')
			{
				strcat(str, temp);
				dibujar = 1;
			}
		}
	}
}

int comprobarExistencia(char textSelectId[], int cantidadExistente)
{
	int num = atoi(textSelectId);
	//retorna 0 si no existe el id
	if (num > cantidadExistente || num <= 0)
		return 0;
	return 1;
}

int graficarParGenAM(int tipoInicio)
{
	int retorno = 0, seleccionado = 0;
	int posiciones[] = {1, 2, 3, 8, 13, 4, 9, 14, 5, 10, 15, 6, 11, 16, 7, 12, 17};

	int cursor_select = 0, text_select = 0;
	char tipo_solicitud[16];
	char probabilidad_aparicion[9];
	ETAPAS *text_etapas = (ETAPAS *)calloc(sizeof(ETAPAS), 5);
	DECIMALES *text_desviacion = (DECIMALES *)calloc(sizeof(DECIMALES), 5);
	DECIMALES *text_timepromedio = (DECIMALES *)calloc(sizeof(DECIMALES), 5);

	//inicializa el fondo de la escritura
	al_set_path_filename(path, "arial_narrow_7.ttf");
	ALLEGRO_FONT *fontAreas = al_load_font(al_path_cstr(path, '/'), 20, 0);

	//se carga pantalla de simulacion
	al_set_path_filename(path, "AMParGenerales.png");
	ALLEGRO_BITMAP *pantAMGenerales = al_load_bitmap(al_path_cstr(path, '/'));

	//Tipo 1: Modificar
	if (tipoInicio == 1)
		extraerRegistro(tipo_solicitud, probabilidad_aparicion, text_etapas, text_desviacion, text_timepromedio);
	//Tipo 2: Agregar
	else
	{
		strcpy(probabilidad_aparicion, "");
		strcpy(tipo_solicitud, "");
	}
	while (1)
	{
		//dibuja el fondo y los cuadros de texto
		if (dibujar)
		{
			al_draw_bitmap(pantAMGenerales, 0, 0, 0);
			escribirParametroGeneral(fontAreas, tipo_solicitud, probabilidad_aparicion, text_etapas, text_desviacion, text_timepromedio);
			al_draw_textf(fontAreas, al_map_rgb(0, 0, 0), 539, 160, 0, "%d", idModificar);
			graficarSeleccionadoAMGen(text_select);
			al_flip_display();
			dibujar = 0;
		}
		al_wait_for_event(eventos, &eventoActual);
		if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			retorno = -1;
			break;
		}
		//si se minimiza
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN)
			dibujar = 1;
		//si se saca la ventana de la pantalla
		else if (eventoActual.type == ALLEGRO_EVENT_DISPLAY_EXPOSE)
			dibujar = 1;
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;
			cursor_select = DetectarCuadro(mouseActual.x, mouseActual.y);
			if (cursor_select > 0 && cursor_select < 18)
			{
				al_set_system_mouse_cursor(ventana, 5);
				cambioCursor = 1;
			}
			else if (cursor_select && !cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 17);
				cambioCursor = 1;
			}
			else if (!cursor_select && cambioCursor)
			{
				al_set_system_mouse_cursor(ventana, 1);
				cambioCursor = 0;
			}
		}
		//si el eventoActual es un click de mouse guarda sus coordenadas
		else if (eventoActual.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			mouseActual.x = eventoActual.mouse.x;
			mouseActual.y = eventoActual.mouse.y;

			//si es un click izquierdo guarda en que cuadro dio click -> text_select
			if (eventoActual.mouse.button == 1)
			{
				text_select = DetectarCuadro(mouseActual.x, mouseActual.y);
				for (int i=0; i<17; i++)
				{
					if (text_select==posiciones[i])
					{
						seleccionado = i;
						break;
					}
				}
				//verifica si un boton fue presionado
				if (text_select == 18)
				{
					if (guardarDatos(fontAreas, tipoInicio, tipo_solicitud, probabilidad_aparicion, text_etapas, text_desviacion, text_timepromedio))
					{
						retorno = 3;
						break;
					}
					else
						graficarError(4);
				}
				else if (text_select == 19)
				{
					retorno = 3;
					break;
				}
				else if (text_select != 0)
					dibujar = 1;
			}
		}
		//determina en que cuadro se escribira o si se movio
		else if (eventoActual.type == ALLEGRO_EVENT_KEY_CHAR)
		{
			// tab
			if (eventoActual.keyboard.keycode == 64)
			{
				//shift+tab
				if (eventoActual.keyboard.modifiers == 1 || eventoActual.keyboard.modifiers == 513 || eventoActual.keyboard.modifiers == 1537)
				{
					seleccionado--;
					if (seleccionado < 0)
						seleccionado = 16;
					if (text_select == posiciones[seleccionado])
						seleccionado--;
					text_select = posiciones[seleccionado];
					dibujar = 1;
				}
				//tab
				else
				{
					if (text_select == posiciones[seleccionado])
						seleccionado++;
					if (seleccionado == 17)
						seleccionado = 0;
					text_select = posiciones[seleccionado];
					seleccionado++;
					dibujar = 1;
				}
			}
			//escape
			else if (eventoActual.keyboard.keycode == 59)
			{
				seleccionado = 0;
				if (text_select != 18 && text_select != 19)
					text_select = 18;
				else
					text_select = 1;
				dibujar = 1;
			}
			//enter
			else if (eventoActual.keyboard.keycode == 67)
			{
				if (text_select == 18)
				{
					if (guardarDatos(fontAreas, tipoInicio, tipo_solicitud, probabilidad_aparicion, text_etapas, text_desviacion, text_timepromedio))
					{
						retorno = 3;
						break;
					}
					else
						graficarError(4);
				}
				else if (text_select == 19)
				{
					retorno = 3;
					break;
				}
			}
			//flecha izquierda y derecha
			else if (eventoActual.keyboard.keycode == 82 || eventoActual.keyboard.keycode == 83)
			{
				if (text_select == 18 || text_select == 19)
				{
					if (text_select == 18)
						text_select = 19;
					else
						text_select = 18;
					dibujar = 1;
				}
			}
			if (text_select != 18 && text_select != 19 && text_select != 0)
			{
				switch (text_select)
				{
				//nombre solicitud
				case 1:
					manipularEntrada(eventoActual, tipo_solicitud, 1, longitudesGenerales);
					break;
				//probabilidad de aparicion
				case 2:
					manipularEntrada(eventoActual, probabilidad_aparicion, 3, longitudesGenerales);
					break;
				//columnas areas
				case 3:
					manipularEntrada(eventoActual, text_etapas[0].area, 2, longitudesGenerales);
					break;
				case 4:
					manipularEntrada(eventoActual, text_etapas[1].area, 2, longitudesGenerales);
					break;
				case 5:
					manipularEntrada(eventoActual, text_etapas[2].area, 2, longitudesGenerales);
					break;
				case 6:
					manipularEntrada(eventoActual, text_etapas[3].area, 2, longitudesGenerales);
					break;
				case 7:
					manipularEntrada(eventoActual, text_etapas[4].area, 2, longitudesGenerales);
					break;
				// columnas tiempo promedio
				case 8:
					manipularEntrada(eventoActual, text_timepromedio[0].num, 3, longitudesGenerales);
					break;
				case 9:
					manipularEntrada(eventoActual, text_timepromedio[1].num, 3, longitudesGenerales);
					break;
				case 10:
					manipularEntrada(eventoActual, text_timepromedio[2].num, 3, longitudesGenerales);
					break;
				case 11:
					manipularEntrada(eventoActual, text_timepromedio[3].num, 3, longitudesGenerales);
					break;
				case 12:
					manipularEntrada(eventoActual, text_timepromedio[4].num, 3, longitudesGenerales);
					break;
				//columna desviacion tipica
				case 13:
					manipularEntrada(eventoActual, text_desviacion[0].num, 3, longitudesGenerales);
					break;
				case 14:
					manipularEntrada(eventoActual, text_desviacion[1].num, 3, longitudesGenerales);
					break;
				case 15:
					manipularEntrada(eventoActual, text_desviacion[2].num, 3, longitudesGenerales);
					break;
				case 16:
					manipularEntrada(eventoActual, text_desviacion[3].num, 3, longitudesGenerales);
					break;
				case 17:
					manipularEntrada(eventoActual, text_desviacion[4].num, 3, longitudesGenerales);
					break;
				}
			}
		}
	}
	al_destroy_font(fontAreas);
	al_destroy_bitmap(pantAMGenerales);
	return retorno;
}

void graficarSeleccionadoAMGen(int seleccionado)
{
	al_set_path_filename(path, "Marcador.png");
	ALLEGRO_BITMAP *senhalador = al_load_bitmap(al_path_cstr(path, '/'));

	switch (seleccionado)
	{
	// primeros dos text
	case 1:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 540, 250, 455, 4, 0);
		break;
	case 2:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 540, 312, 455, 4, 0);
		break;
	//columnas de areas
	case 3:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 538, 456, 75, 4, 0);
		break;
	case 4:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 634, 456, 75, 4, 0);
		break;
	case 5:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 731, 456, 75, 4, 0);
		break;
	case 6:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 828, 456, 75, 4, 0);
		break;
	case 7:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 925, 456, 75, 4, 0);
		break;
	//columnas tiempo promedio
	case 8:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 538, 513, 75, 4, 0);
		break;
	case 9:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 634, 513, 75, 4, 0);
		break;
	case 10:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 731, 513, 75, 4, 0);
		break;
	case 11:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 828, 513, 75, 4, 0);
		break;
	case 12:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 925, 513, 75, 4, 0);
		break;
	//columna de desviacion tipica
	case 13:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 538, 569, 75, 4, 0);
		break;
	case 14:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 634, 569, 75, 4, 0);
		break;
	case 15:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 731, 569, 75, 4, 0);
		break;
	case 16:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 828, 569, 75, 4, 0);
		break;
	case 17:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 925, 569, 75, 4, 0);
		break;
	// botones
	case 18:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 445, 667, 143, 4, 0);
		break;
	case 19:
		al_draw_scaled_bitmap(senhalador, 0, 0, 135, 4, 796, 667, 143, 4, 0);
		break;
	}
	al_destroy_bitmap(senhalador);
}

void escribirParametroGeneral(ALLEGRO_FONT *fontAreas, char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[])
{
	if (strlen(tipo_solicitud) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 539, 223, 0, tipo_solicitud);

	if (strlen(probabilidad_aparicion) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 539, 283, 0, probabilidad_aparicion);

	//columna de areas
	if (strlen(text_etapas[0].area) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 566, 425, 0, text_etapas[0].area);
	if (strlen(text_etapas[1].area) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 665, 425, 0, text_etapas[1].area);
	if (strlen(text_etapas[2].area) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 762, 425, 0, text_etapas[2].area);
	if (strlen(text_etapas[3].area) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 858, 425, 0, text_etapas[3].area);
	if (strlen(text_etapas[4].area) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 954, 425, 0, text_etapas[4].area);

	//COLUMNAS DE TIEMPO PROMEDIO
	if (strlen(text_timepromedio[0].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 536, 480, 0, text_timepromedio[0].num);
	if (strlen(text_timepromedio[1].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 633, 480, 0, text_timepromedio[1].num);
	if (strlen(text_timepromedio[2].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 730, 480, 0, text_timepromedio[2].num);
	if (strlen(text_timepromedio[3].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 826, 480, 0, text_timepromedio[3].num);
	if (strlen(text_timepromedio[4].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 924, 480, 0, text_timepromedio[4].num);

	//COLUMNAS DE DESVIACION TIPICA
	if (strlen(text_desviacion[0].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 536, 535, 0, text_desviacion[0].num);
	if (strlen(text_desviacion[1].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 632, 535, 0, text_desviacion[1].num);
	if (strlen(text_desviacion[2].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 730, 535, 0, text_desviacion[2].num);
	if (strlen(text_desviacion[3].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 826, 535, 0, text_desviacion[3].num);
	if (strlen(text_desviacion[4].num) > 0)
		al_draw_text(fontAreas, al_map_rgb(0, 0, 0), 924, 535, 0, text_desviacion[4].num);
}

int DetectarCuadro(int x, int y)
{
	int retorno = 0;

	// primeros dos text
	if (x <= (1004) && x >= (537) && y <= (246) && y >= (220))
		retorno = 1;
	else if (x <= (1004) && x >= (537) && y <= (307) && y >= (280))
		retorno = 2;
	//columnas de areas
	else if (x <= (615) && x >= (537) && y <= (453) && y >= (418))
		retorno = 3;
	else if (x <= (710) && x >= (631) && y <= (453) && y >= (418))
		retorno = 4;
	else if (x <= (808) && x >= (728) && y <= (453) && y >= (418))
		retorno = 5;
	else if (x <= (904) && x >= (824) && y <= (453) && y >= (418))
		retorno = 6;
	else if (x <= (1001) && x >= (920) && y <= (453) && y >= (418))
		retorno = 7;
	//columnas tiempo promedio
	else if (x <= (615) && x >= (537) && y <= (504) && y >= (471))
		retorno = 8;
	else if (x <= (710) && x >= (631) && y <= (504) && y >= (471))
		retorno = 9;
	else if (x <= (808) && x >= (728) && y <= (504) && y >= (471))
		retorno = 10;
	else if (x <= (904) && x >= (824) && y <= (504) && y >= (471))
		retorno = 11;
	else if (x <= (1001) && x >= (920) && y <= (504) && y >= (471))
		retorno = 12;
	//columna de desviacion tipica
	else if (x <= (615) && x >= (537) && y <= (565) && y >= (530))
		retorno = 13;
	else if (x <= (710) && x >= (631) && y <= (565) && y >= (530))
		retorno = 14;
	else if (x <= (808) && x >= (728) && y <= (565) && y >= (530))
		retorno = 15;
	else if (x <= (904) && x >= (824) && y <= (565) && y >= (530))
		retorno = 16;
	else if (x <= (1001) && x >= (920) && y <= (565) && y >= (530))
		retorno = 17;
	// botones
	else if (x <= (595) && x >= (439) && y <= (662) && y >= (624))
		retorno = 18;
	else if (x <= (946) && x >= (790) && y <= (662) && y >= (624))
		retorno = 19;
	return retorno;
}

void extraerRegistro(char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[])
{
	strcpy(tipo_solicitud, tiposDeSolicitud[idModificar - 1].nom_soli);
	sprintf(probabilidad_aparicion, "%.5lf", tiposDeSolicitud[idModificar - 1].prob_soli);
	for (int i = 0; i < 5; i++)
	{
		//areas
		sprintf(text_etapas[i].area, "%d", tramites[idModificar - 1].areas[i].nro_area);
		//desviacion tipica
		datosToString(&text_desviacion[i], tramites[idModificar - 1].areas[i].desv_tipica);
		//tiempo promedio
		datosToString(&text_timepromedio[i], tramites[idModificar - 1].areas[i].tiempo_prom);
	}
}

void datosToString(DECIMALES *text, double numero)
{
	if (numero < 10)
		sprintf(text->num, "%.6f", numero);
	else if (numero < 100)
		sprintf(text->num, "%.5f", numero);
	else if (numero < 1000)
		sprintf(text->num, "%.4f", numero);
	else if (numero < 10000)
		sprintf(text->num, "%.3f", numero);
	else if (numero < 100000)
		sprintf(text->num, "%.2f", numero);
	else if (numero < 1000000)
		sprintf(text->num, "%.1f", numero);
	else
		sprintf(text->num, "%.0f", numero);
}

int guardarDatos(ALLEGRO_FONT *fontAreas, int esNuevo, char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[])
{

	if (comprobarDatos(tipo_solicitud, probabilidad_aparicion, text_etapas, text_desviacion, text_timepromedio, esNuevo) == 0)
		return 0;

	double nuevaProb = atof(probabilidad_aparicion);
	double valor;
	double total;
	//Si es un nuevo tipo de Solicitud, añadimos un espacio los vectores
	if (esNuevo == 2)
	{
		cantTipoSoli++;
		tiposDeSolicitud = realloc(tiposDeSolicitud, sizeof(TipoSolicitud) * cantTipoSoli);
		tramites = realloc(tramites, sizeof(Tramite) * cantTipoSoli);
		tipoSolicitudesModif = realloc(tipoSolicitudesModif, sizeof(char) * cantTipoSoli);
		valor = -nuevaProb;
		iniciarTipoSoliModificados();
		tipoSolicitudesModif[idModificar - 1] = '1';
		++cantProbModif;
		sumaProbModif = nuevaProb;
		total = 1;
	}
	else
	{
		valor = tiposDeSolicitud[idModificar - 1].prob_soli - nuevaProb;
		if (tipoSolicitudesModif[idModificar - 1] == '0' && valor != 0)
		{
			sumaProbModif += nuevaProb;
			cantProbModif++;
			if (cantProbModif == cantTipoSoli)
				iniciarTipoSoliModificados();
			if (cantTipoSoli != 2 || (cantTipoSoli == 2 && cantProbModif - 1 == 0))
				tipoSolicitudesModif[idModificar - 1] = '1';
		}
		else
			sumaProbModif += -valor;
		total = 0;
		//Inicializamos de nuevo nuestras variables de validacion ya que ese era la ultima prob a modificar
	}

	//Actualizamos los nuevos valores ingresados por el usuario
	tiposDeSolicitud[idModificar - 1].id_tipo_soli = idModificar;
	strcpy(tiposDeSolicitud[idModificar - 1].nom_soli, tipo_solicitud);

	if (cantTipoSoli != 1)
		tiposDeSolicitud[idModificar - 1].prob_soli = nuevaProb;
	else
		tiposDeSolicitud[idModificar - 1].prob_soli = 1;

	//Validamos que se haya tocado la prob del que se va a modificar para reajustar o si es nuevo el tipo de Solicitud
	if (cantTipoSoli != 1 && (valor != 0 || esNuevo == 2))
		ajustarProbabilidad(tiposDeSolicitud, cantTipoSoli, valor, total, tipoSolicitudesModif);

	tramites[idModificar - 1].id_tipo_soli = idModificar;
	for (int i = 0; i < 5; ++i)
	{
		if (text_etapas[i].area[0] != '\0')
		{
			tramites[idModificar - 1].areas[i].nro_area = atoi(text_etapas[i].area);
			tramites[idModificar - 1].areas[i].desv_tipica = atof(text_desviacion[i].num);
			tramites[idModificar - 1].areas[i].tiempo_prom = atof(text_timepromedio[i].num);
		}
		else
			memset(&tramites[idModificar - 1].areas[i], 0, sizeof(Area));
	}
	escribirTipoDeSolicitudes(tiposDeSolicitud, cantTipoSoli);
	escribirTramites(tramites, cantTipoSoli);
	return 1;
}

int comprobarDatos(char tipo_solicitud[], char probabilidad_aparicion[], ETAPAS text_etapas[], DECIMALES text_desviacion[], DECIMALES text_timepromedio[], int esNuevo)
{
	if (tipo_solicitud[0] == '\0' || probabilidad_aparicion[0] == '\0')
		return 0;

	if (atof(probabilidad_aparicion) <= 0 || atof(probabilidad_aparicion) >= 1)
		return 0;
	else
	{
		double numvalid;
		if (esNuevo != 2)
		{
			if (tipoSolicitudesModif[idModificar - 1] == '1')
				numvalid = sumaProbModif - tiposDeSolicitud[idModificar - 1].prob_soli;
			else if (cantProbModif + 1 != cantTipoSoli)
				numvalid = sumaProbModif;
			else
				numvalid = 0;
		}
		else
			numvalid = 0;
		if (cantProbModif >= 1 && atof(probabilidad_aparicion) + numvalid >= 1 && atof(probabilidad_aparicion) != tiposDeSolicitud[idModificar - 1].prob_soli)
			return 0;
	}

	//comprueba los errores de las etapas
	int con = 0;
	int areas[] = {0, 0, 0, 0, 0};
	for (int i = 0; i < 5; i++)
	{
		//si area igual a cero la columna debe ser igual a cero
		if (esVacio(text_etapas[i].area, 1) && ((!esVacio(text_desviacion[i].num, 0) && atof(text_desviacion[i].num) != 0) || !esVacio(text_timepromedio[i].num, 1)))
			return 0;
		//si area es distinto la columna debe ser igual a cero
		if (!esVacio(text_etapas[i].area, 1) && (esVacio(text_desviacion[i].num, 0) || esVacio(text_timepromedio[i].num, 1)))
			return 0;
		//comprobar que no todas las areas sean igual a null al mismo tiempo
		if (text_etapas[i].area[0] == '\0')
			con++;
		//comprueba que no haya areas repetidas
		else if (text_etapas[i].area[0] != '0')
		{
			if (areas[atoi(text_etapas[i].area) - 1] == 0)
				areas[atoi(text_etapas[i].area) - 1] = 1;
			else
				return 0;
			//Validamos los datos
			if (atof(text_timepromedio[i].num) <= 0)
				return 0;
			if (atof(text_desviacion[i].num) < 0 || atof(text_desviacion[i].num) > atof(text_timepromedio[i].num))
				return 0;
		}
	}
	if (con == 5)
		return 0;
	return 1;
}

int comprobarParEsp(char *codigoSim, char *duracionSim, char *cantSoli, char *intervaloAct)
{
	//si alguno esta vacio es invalido
	if (codigoSim[0] == '\0' || duracionSim[0] == '\0' || cantSoli[0] == '\0' || intervaloAct[0] == '\0')
		return 0;
	//ningun parametro puede valer 0
	params.cant_soli = atoi(cantSoli);
	if (params.cant_soli <= 0)
		return 0;
	params.dur_sim = atof(duracionSim);
	if (params.dur_sim <= 0 || params.dur_sim > 24)
		return 0;
	params.inter_act = atof(intervaloAct);
	if (params.inter_act <= 0 || params.inter_act / 60 > params.dur_sim)
		return 0;
	strcpy(params.cod_sim, codigoSim);
	return 1;
}

//Inicializa las variables utilizadas para el reajuste de probabilidades
void iniciarTipoSoliModificados()
{
	memset(tipoSolicitudesModif, '0', cantTipoSoli);
	sumaProbModif = 0;
	cantProbModif = 0;
}