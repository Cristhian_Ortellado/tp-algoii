#include "parametrogeneral.h"

int main(int argc, char const *argv[])
{
	int cantTipoSoli = 3;
	int opt = 1, opcion, i = 0;
	TipoSolicitud arraytipoSoli[3] = {{1, "Soli 1", 0.30}, {2, "Soli 2", 0.30}, {3, "Soli 3", 0.40}};
	escribirTipoDeSolicitudes(arraytipoSoli, cantTipoSoli);
	leerTipoDeSolicitudes(cantTipoSoli);

	//Temporal
	Tramite *tramites = (Tramite *)calloc(sizeof(Tramite), cantTipoSoli);

	tramites[0].id_tipo_soli = 1;
	tramites[0].areas[0].nro_area = 3;
	tramites[0].areas[0].tiempo_prom = 11;
	tramites[0].areas[0].desv_tipica = 0.05;

	tramites[0].areas[1].nro_area = 1;
	tramites[0].areas[1].tiempo_prom = 11;
	tramites[0].areas[1].desv_tipica = 0.03;

	tramites[0].areas[2].nro_area = 4;
	tramites[0].areas[2].tiempo_prom = 11;
	tramites[0].areas[2].desv_tipica = 0.01;

	tramites[1].id_tipo_soli = 2;
	tramites[1].areas[0].nro_area = 5;
	tramites[1].areas[0].tiempo_prom = 11;
	tramites[1].areas[0].desv_tipica = 0.15;

	tramites[1].areas[1].nro_area = 4;
	tramites[1].areas[1].tiempo_prom = 11;
	tramites[1].areas[1].desv_tipica = 0.35;

	tramites[1].areas[2].nro_area = 2;
	tramites[1].areas[2].tiempo_prom = 11;
	tramites[1].areas[2].desv_tipica = 0.71;

	tramites[2].id_tipo_soli = 3;
	tramites[2].areas[0].nro_area = 1;
	tramites[2].areas[0].tiempo_prom = 11;
	tramites[2].areas[0].desv_tipica = 0.12;

	tramites[2].areas[1].nro_area = 2;
	tramites[2].areas[1].tiempo_prom = 11;
	tramites[2].areas[1].desv_tipica = 0.35;

	tramites[2].areas[2].nro_area = 3;
	tramites[2].areas[2].tiempo_prom = 11;
	tramites[2].areas[2].desv_tipica = 0.05;

	tramites[2].areas[3].nro_area = 4;
	tramites[2].areas[3].tiempo_prom = 11;
	tramites[2].areas[3].desv_tipica = 0.71;
	escribirTramites(tramites, cantTipoSoli);
	leerTramites(cantTipoSoli);
	/*while(opt)
	{
		printf("0- Salir\n1- Agregar\n2- Borrar\n3- Modificar\n");
		scanf("%d",&opt);
		i=0;
		if(opt==1)
		{
			printf("Solicitudes actuales: %d\nId: ", cantTipoSoli);
			scanf("%d", &opcion);
			if(opcion>0)
			{	//hay que utilizar realloc para redimensionar el vector cuando se quiera ingresar mas campos
				cantTipoSoli++;		//se agrega un nuevo campo
				tramites = (Tramite *)realloc(tramites, (cantTipoSoli)*sizeof(Tramite));
				memset(&tramites[opcion-1], 0, sizeof(Tramite));		//inicializa a 0 el nuevo espacio
				tramites[opcion-1].id_tipo_soli=opcion;
				while(i<5)
				{
					printf("Ingrese area, tiempo promedio y desviacion tipica, area=0 terminar\n");
					scanf("%d", &tramites[opcion-1].areas[i].nro_area);
					if(tramites[opcion-1].areas[i].nro_area!=0)
					{	//lf para double
						scanf("%lf%lf", &tramites[opcion-1].areas[i].tiempo_prom, &tramites[opcion-1].areas[i].desv_tipica);
					}
					else
						i=5;
					i++;
				}
			}
			else
				printf("Id invalido\n");
			opcion=1;
		}
		else if(opt==2)
			eliminarTramites(tramites,cantTipoSoli);
		else if(opt==3)
			editarTramites(tramites,cantTipoSoli);
		escribirTramites(tramites,cantTipoSoli);		//escribe todo en el archivo, se deberia de tener una funcion actualizar para cada caso
		leerTramites(cantTipoSoli);
	}*/
	return 0;
}
