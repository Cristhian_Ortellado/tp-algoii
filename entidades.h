
#ifndef ENTIDADES_FILE //Para poder hacer include desde dos o mas archivos diferentes
#define ENTIDADES_FILE

typedef struct
{
	int id_tipo_soli;  //Identificador de tipo de solicitud
	char nom_soli[30]; //Nombre del tipo de solicitud
	double prob_soli;  //Probabilidad de aparacion
} TipoSolicitud;

typedef struct
{
	int nro_soli;		  //Numero secuencial asociado a la solicitud
	int tipo_soli;		  //Identificador de tipo de solicitud
	double hora_entrada;	//Hora en que se recibió la solicutud al inicio
	double hora_entrada_area; //Hora de entrada de la solicitud en el area actual
	double hora_salida;	 //Hora en que se terminó de procesar la solicitud
	int indice_area;		  //Indice en el array de area de tramites, para saber en que area está siendo procesada actualmente
	char procesada;		  // 1 si fue procesada totalmente, 0 si no fue procesada o fue procesada parcialmente

} Solicitud;

typedef struct
{
	char cod_sim[5];  //Código de simulación
	double dur_sim;   //Duración de simulaciòn en horas
	int cant_soli;    //Cantidad de solicitudes durante la simulaciòn
	double inter_act; //Intervalo de actualizacion en minutos
} ParametrosEspecificos;

typedef struct Area
{
	int nro_area;
	double tiempo_prom;
	double desv_tipica;
} Area;

typedef struct Tramite
{
	int id_tipo_soli;
	Area areas[5];
} Tramite;

//Cola
typedef struct nodo
{
	Solicitud *solicitud;
	struct nodo *sig;
	struct nodo *atras;
} Nodo;

typedef struct
{
	int *cantSoliProcesadasPorTipo; //Array Cantidad de solicitudes procesadas del area por cada tipo
	int *cantSoliPorTipo;		  //Cantidad de solicitudes actuales en cola por tipo
	double horaSalidaUlt;		  //Guarda la hora de salida de la ultima solicitud procesada
	int cantTotalSoli;
	int cantTotalSoliProc;
	Nodo *frente;
	Nodo *final;
} AreaCola;

#endif