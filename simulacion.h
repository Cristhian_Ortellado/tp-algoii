#include <stdio.h>
#include "utilidades.h"
#include "areasColas.h"
#include <time.h>
#include <gsl/gsl_rng.h>
#include "parametrogeneral.h"
#include "informes.h"
#include "stdlib.h"

Solicitud *crearSolicitud(int idSolicitud, double tiempoDeLLegada, int tipodeSolicitud);
int determinarTipoSolicitud(TipoSolicitud *tipoSoli, int cantTipoSoli);
int compare_float(double x, double y);
void inicializarParametrosGenerales();

AreaCola *areasColas;
TipoSolicitud *tiposDeSolicitud;
Solicitud **solicitudes;
Tramite *tramites;
ParametrosEspecificos params;

//Cantidad de tipo de solicitudes existentes
int cantTipoSoli;
gsl_rng *r;

int cantMax;
double miuPoisson;

char *reloj = NULL;

//Cantidad de solicitudes que llegaron en cierto intervalo
int cantSoliNuevas;

//Cantidad de solicitudes que fueron procesadas en cierto intervalo
int cantSoliSalientes;

//Indice del vector para las nuevas solicitudes
int indiceSolicitud;

//Para el manejo de los intervalos
double tiempoInicial;
double tiempoFinal;
double maxTiempoFinal;

//Cantidad total de solicitudes actuales
int cantTotalSolis;
int cantTotalPendientes;
int cantTotalProcesadas;

void inicializarParametrosGenerales()
{
	verificarDirectorios();
	//Obtenemos la cantidad de tipos de solicitudes en el archivo de parametros
	cantTipoSoli = obtenerCantTipoDeSolicitudes();
	if(cantTipoSoli==0)
		return ;
	//Obtenemos todos los tipos de solicitudes
	tiposDeSolicitud = leerTipoDeSolicitudes(cantTipoSoli);

	//Obtenemos todos los tramites de cada tipo de solicitud
	tramites = leerTramites(cantTipoSoli);
}

void inicializarParametrosSimulacion()
{
	r = gsl_rng_alloc(gsl_rng_taus);
	gsl_rng_set(r, time(NULL));
	srand(time(NULL));
	//Creamos las colas para cada área;
	areasColas = crearColas(cantTipoSoli);
	//Vector que contiene las solicitudes que van llegando con el transcurso del tiempo
	solicitudes = (Solicitud **)malloc(sizeof(Solicitud *) * params.cant_soli);

	cantMax = params.cant_soli;

	maxTiempoFinal = params.dur_sim * 60;

	miuPoisson = (double)params.cant_soli / params.dur_sim;

	miuPoisson /= 60.0;

	indiceSolicitud = 0;

	//Extremo izquierdo del intervalo de actualizacion
	tiempoInicial = 0;

	//Extremo derecho del intervalo de actualizacion
	tiempoFinal = 0;
	cantSoliNuevas = 0;
	cantSoliSalientes = 0;
	cantTotalSolis = 0;

	//inicializar para el informe
	cantTotalProcesadas = 0;
	cantTotalPendientes = 0;

	reloj = getHHMMSS(0);
}

int obtenerInfoSimulacion()
{

	int continuar = 0;
	if (tiempoFinal < maxTiempoFinal)
	{
		continuar = 1;
		if (tiempoInicial + params.inter_act > maxTiempoFinal)
			tiempoFinal = maxTiempoFinal;
		else
		{
			tiempoFinal += params.inter_act;
		}
		reloj = getHHMMSS(tiempoFinal * 60);

		// printf("Entre los %s  y %s: \n", getHHMMSS(tiempoInicial * 60), reloj);

		//Obtenemos la cantidad de solicitudes que llegan en ese tiempo con Poisson

		if (tiempoFinal == maxTiempoFinal)
			cantSoliNuevas = cantMax;
		else
			cantSoliNuevas = getCantSoli(r, miuPoisson, params.inter_act, cantMax);

		//Restamos la cantidad de solicitudes que llegaron en el periodo de tiempo
		cantMax -= cantSoliNuevas;
		//Tiempo relativo que corresponde a los 0 minutos y el intervalo de actulizacion
		double tiempoActualRelativo = 0;
		for (int i = 0; i < cantSoliNuevas; i++)
		{
			//Determinamos para cada k solicitudes, de que tipo es
			int tipoSoli = determinarTipoSolicitud(tiposDeSolicitud, cantTipoSoli);

			//El maximo tiempo entre solicitudes
			double maxTiempoRelativo = tiempoFinal - tiempoInicial;
			double tiempoEntreSoli;
			if (compare_float(tiempoActualRelativo, maxTiempoRelativo))
				tiempoEntreSoli = 0;
			else
			{
				tiempoEntreSoli = getTiempoEntreSoli(r, miuPoisson, tiempoActualRelativo, maxTiempoRelativo);
			}
			//printf("Tiempo entre SOLI = %f\n",tiempoEntreSoli);

			tiempoActualRelativo += tiempoEntreSoli;

			Solicitud *nuevaSolicitud = crearSolicitud((indiceSolicitud++) + 1, tiempoActualRelativo + tiempoInicial, tipoSoli + 1);
			// printf("Solicitud %d, Tipo:%d,  Hora Llegada: %s\n", nuevaSolicitud->nro_soli, nuevaSolicitud->tipo_soli, getHHMMSS(nuevaSolicitud->hora_entrada * 60));
			//Obtenemos el numero de area en la que se va a insertar de acuerdo a su tipo

			int nroArea = tramites[tipoSoli].areas[nuevaSolicitud->indice_area].nro_area;

			//Insertamos en la cola del area correspondiente
			ingresarSolicitud(&areasColas[nroArea - 1], nuevaSolicitud);

			solicitudes[nuevaSolicitud->nro_soli - 1] = nuevaSolicitud;
		}
		//printf("Tiempo Total relativo: %f\n",tiempoActualRelativo);

		//Se procede a procesar una solicitud de cada area
		cantSoliSalientes = quitarProcesados(r, areasColas, tramites, tiempoFinal);
		cantTotalSolis += cantSoliNuevas - cantSoliSalientes;
		// imprimirActualizacion(areasColas, tramites, cantSoliNuevas);
		tiempoInicial = tiempoFinal;
	}
	else
	{
		crearInforme(&params, solicitudes, areasColas, tiposDeSolicitud, tramites, cantTipoSoli, &cantTotalPendientes, &cantTotalProcesadas);
	}
	return continuar;
}

Solicitud *crearSolicitud(int idSolicitud, double tiempoDeLlegada, int tipodeSolicitud)
{
	Solicitud *nuevaSolicitud = (Solicitud *)calloc(sizeof(Solicitud), 1);
	nuevaSolicitud->nro_soli = idSolicitud;
	nuevaSolicitud->hora_entrada = tiempoDeLlegada;
	nuevaSolicitud->hora_salida = tiempoDeLlegada;
	nuevaSolicitud->indice_area = 0;
	nuevaSolicitud->tipo_soli = tipodeSolicitud;
	return nuevaSolicitud;
}

int determinarTipoSolicitud(TipoSolicitud *tipoSoli, int cantTipoSoli)
{
	double prob = ((double)rand() * (1.0 - 0.0)) / (double)RAND_MAX + 0.0;
	//Extremo izquierdo del intervalo cerrado
	double extremoIzq = 0;
	//Extremo derecho del intervalo cerrado
	double extremoDer = 0;
	int tipo;

	//Recorremos el array de Tipo de Solicitudes para determinar el tipo de cada solicitud
	for (tipo = 0; tipo < cantTipoSoli; tipo++)
	{
		//Colocamos los extremos del intervalo de la probabilidad
		extremoIzq = extremoDer;
		extremoDer = extremoIzq + tipoSoli[tipo].prob_soli;

		if (prob >= extremoIzq && prob <= extremoDer)
			break;
	}
	return tipo;
}
