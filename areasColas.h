#include "cola.h"
#include "estadistica.h"

void ingresarSolicitud(AreaCola *areaCola, Solicitud *solicitud);

AreaCola *crearColas(int cantTipoSoli)
{
	AreaCola *areasColas = (AreaCola *)malloc(sizeof(AreaCola) * 5);
	for (int i = 0; i < 5; ++i)
		inicializarCola(&areasColas[i], cantTipoSoli);
	return areasColas;
}

int quitarProcesados(const gsl_rng *r, AreaCola *areaColas, Tramite *tramites, int tiempoAbsoluto)
{
	int cantDesencolados = 1;
	int salientes = 0;
	while (cantDesencolados > 0)
	{
		cantDesencolados = 0;
		for (int i = 0; i < 5; i++)
		{
			if (!colaVacia(&areaColas[i]))
			{
				Solicitud *solicitud = areaColas[i].frente->solicitud;
				//Si la hora de entrada de la solicitud al area es igual a la hora de salida de la solicitud, es una solicitud pendiente del intervalo anterior
				int tipoSoli = solicitud->tipo_soli - 1;
				if (solicitud->hora_entrada_area == solicitud->hora_salida)
				{
					//Se procede a calcular el tiempo a ser procesado en esa area

					int indiceArea = solicitud->indice_area;
					double desviacionTipica = tramites[tipoSoli].areas[indiceArea].desv_tipica;
					double tiempoPromedio = tramites[tipoSoli].areas[indiceArea].tiempo_prom;

					//Quitamos el tiempo que tardo la solicitud en ser procesada mediante distribucion normal
					double tiempoProceso = getTiempoProcesamiento(r, desviacionTipica, tiempoPromedio);

					if (solicitud->hora_entrada_area < areaColas[i].horaSalidaUlt)
					{
						solicitud->hora_salida = areaColas[i].horaSalidaUlt + tiempoProceso;
					}
					else
					{
						solicitud->hora_salida += tiempoProceso;
					}
				}
				if (solicitud->hora_salida <= tiempoAbsoluto)
				{
					//printf("Solicitud %d, Salida del Area %d: %s\n",solicitud->nro_soli ,i+1,getHHMMSS(solicitud->hora_salida*60));
					++cantDesencolados;
					desencolar(&areaColas[i]);
					areaColas[i].horaSalidaUlt = solicitud->hora_salida;
					int siguienteArea = solicitud->indice_area + 1;

					//Validamos que la solicitud deba ir a otra area para ser procesada
					if (siguienteArea <= 4 && tramites[tipoSoli].areas[siguienteArea].nro_area != 0)
					{
						int nroArea = tramites[tipoSoli].areas[siguienteArea].nro_area;
						//Mandamos la solicitud a la siguiente area
						ingresarSolicitud(&areaColas[nroArea - 1], solicitud);
						//printf("Proxima area %d\n",nroArea);
					}
					else
					{
						solicitud->procesada = '1';
						++salientes;
					}

					//Aumentamos el contador de cantidad de solicitudes de tal tipo que fue procesada por la area actual
					++areaColas[i].cantSoliProcesadasPorTipo[tipoSoli];
					++areaColas[i].cantTotalSoliProc;
					--areaColas[i].cantSoliPorTipo[tipoSoli];
					--areaColas[i].cantTotalSoli;

					solicitud->indice_area++;
				}
			}
		}
	}
	return salientes;
}

void ingresarSolicitud(AreaCola *areaCola, Solicitud *solicitud)
{
	//Incrementamos el contador de solicitudes encolados
	++areaCola->cantSoliPorTipo[solicitud->tipo_soli - 1];
	++areaCola->cantTotalSoli;
	solicitud->hora_entrada_area = solicitud->hora_salida;
	insertar(areaCola, solicitud);
}

void imprimirActualizacion(AreaCola *areaColas, Tramite *tramites, int cantSoli)
{
	printf("Cantidad de solicitudes que llegaron en el intervalo %d\n", cantSoli);
	for (int i = 0; i < 5; i++)
	{
		printf("Area %d\n", i + 1);
		imprimirArea(&areaColas[i]);
	}
	printf("\n");
}