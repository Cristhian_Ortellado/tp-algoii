
#ifndef UTILIDADES_FILE //Para poder hacer include desde dos o mas archivos diferentes
#define UTILIDADES_FILE

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "entidades.h"

#define FILE_TRAMITE "parametros/tramites.txt"
#define FILE_TIPO_SOLICITUDES "parametros/tipoSolicitudes.txt"
#define FILE_TIPO_ULTSIMULACION "parametros/ultimaSimulacion.txt"
#define FILE_INFORME "informes/resultados"
#define T_HHMMSS 9

char *getHHMMSS(double tiempo_en_segundos)
{
	char *hhmmss = (char *)malloc(sizeof(char) * T_HHMMSS);

	//pasa los segundos a horas, minutos y segundos correspondientes;
	int Tiempo_horas = tiempo_en_segundos / (60 * 60);
	int Tiempo_minutos = (tiempo_en_segundos - Tiempo_horas * 60 * 60) / 60;
	int Tiempo_segundos = (tiempo_en_segundos - Tiempo_horas * 60 * 60 - Tiempo_minutos * 60);
	snprintf(hhmmss, T_HHMMSS, "%02d:%02d:%02d", Tiempo_horas, Tiempo_minutos, Tiempo_segundos);

	return hhmmss;
}

char *obtenerNombreInforme(char *codSimulacion)
{
	char *filename = malloc(sizeof(char) * 100);
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	//se genera el nombre del informe con el codigo de simulacion, la fecha y la hora
	snprintf(filename, 100, "%s%s-%4d%02d%02d-%02d%02d%02d.txt", FILE_INFORME, codSimulacion, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	return filename;
}

void verificarDirectorios()
{
	//Verificamos que existan los directorios, si no existen los creamos
	struct stat st = {0};
#ifdef _WIN32
	mkdir("parametros");
	mkdir("informes");
#elif __unix__
	if (stat("informes", &st) == -1)
	{
		mkdir("informes", 0700);
	}
	if (stat("parametros", &st) == -1)
	{
		mkdir("parametros", 0700);
	}
#endif
}

void ordenarSolicitudes(Solicitud *a[], int primero, int ultimo)
{
	int i, j;
	i = primero;
	j = ultimo;
	Solicitud *pivote = a[(i + j) / 2];
	do
	{
		while (a[i]->procesada > pivote->procesada || (a[i]->procesada == pivote->procesada && a[i]->nro_soli < pivote->nro_soli))
			i++;
		while (a[j]->procesada < pivote->procesada || (a[j]->procesada == pivote->procesada && a[j]->nro_soli > pivote->nro_soli))
			j--;
		if (i <= j)
		{
			Solicitud *aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			i++;
			j--;
		}
	} while (i <= j);

	if (primero < j)
		ordenarSolicitudes(a, primero, j);
	if (i < ultimo)
		ordenarSolicitudes(a, i, ultimo);
}

int compare_float(double x, double y)
{
	double epsilon = 0.0000001f;
	if (fabs(x - y) < epsilon)
		return 1;
	return 0;
}

int esVacio (char* str, int comprobarCero){
	if(comprobarCero)
		return (strcmp(str,"")==0 || atof(str) ==0 );
	else
		return (strcmp(str,"")==0 );
}

#endif