#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utilidades.h"
#include "entidades.h"

char *filename;

void crearInforme(ParametrosEspecificos *params, Solicitud *solicitudes[], AreaCola *areasCola, TipoSolicitud *tiposDeSolicitud, Tramite *tramites, int cantTipoSoli, int *cantTotalPendientes, int *cantTotalProcesadas);
void escribirEncabezado(FILE *file, ParametrosEspecificos *params);
void escribirSolicitudes(FILE *file, Solicitud *solicitudes[], Tramite *tramites, int cantSolicitudes);
void escribirAreas(FILE *file, AreaCola *areasCola, TipoSolicitud *tiposSolicitud, int cantTipoSoli, int *cantTotalPendientes, int *cantTotalProcesadas);

void crearInforme(ParametrosEspecificos *params, Solicitud *solicitudes[], AreaCola *areasCola, TipoSolicitud *tiposDeSolicitud, Tramite *tramites, int cantTipoSoli, int *cantTotalPendientes, int *cantTotalProcesadas)
{
	FILE *ptrguardado;

	filename = obtenerNombreInforme(params->cod_sim);
	if ((ptrguardado = fopen(filename, "w+")) == NULL)
	{
		printf("Error al guardar los resultados");
	}
	else
	{
		escribirEncabezado(ptrguardado, params);
		escribirSolicitudes(ptrguardado, solicitudes, tramites, params->cant_soli);
		escribirAreas(ptrguardado, areasCola, tiposDeSolicitud, cantTipoSoli,cantTotalPendientes, cantTotalProcesadas);
		fclose(ptrguardado);
	}
}

void escribirEncabezado(FILE *file, ParametrosEspecificos *params)
{
	fprintf(file, "%-45s%s\n", "Código de simulación:", params->cod_sim);
	fprintf(file, "%-45s%.2f\n", "Duración de simulación en horas:", params->dur_sim);
	fprintf(file, "%-43s%d\n", "Cantidad de solicitudes:", params->cant_soli);
	fprintf(file, "%-44s%.2f\n", "Intervalo de actualizacón en minutos:", params->inter_act);
	fprintf(file, "\nResultados:\n");
}

void escribirSolicitudes(FILE *file, Solicitud *solicitudes[], Tramite *tramites, int cantSolicitudes)
{
	ordenarSolicitudes(solicitudes, 0, cantSolicitudes - 1);

	fprintf(file, "\nSolicitudes Procesadas:\n");
	fprintf(file, "%20s%20s%25s%25s%30s\n", "Número de Solicitud", "Tipo de Solicitud", "HHMMSS de recepción", "HHMMSS de conclusión", "Duración total del proceso "); //se crea el titulo
	int i;

	for (i = 0; i < cantSolicitudes && solicitudes[i]->procesada == '1'; i++)
	{
		Solicitud *soli = solicitudes[i];
		//Duracion del proceso de una solicitud en segundos
		double duracionProceso = (soli->hora_salida - soli->hora_entrada) * 60;
		fprintf(file,"%10d%20d%25s%25s%27.3f\n", soli->nro_soli, soli->tipo_soli, getHHMMSS(soli->hora_entrada * 60), getHHMMSS(soli->hora_salida * 60), duracionProceso);
	}

	fprintf(file, "\nSolicitudes Pendientes:\n");
	fprintf(file, "%20s%20s%25s%25s\n", "Número de Solicitud", "Tipo de Solicitud", "HHMMSS de recepción", "Última Área"); //se crea el titulo

	for (; i < cantSolicitudes; i++)
	{
		Solicitud *soli = solicitudes[i];
		//Ultima area en la que se quedo pendiente
		int nroArea = tramites[soli->tipo_soli - 1].areas[soli->indice_area].nro_area;
		fprintf(file, "%10d%20d%25s%27d\n", soli->nro_soli, soli->tipo_soli, getHHMMSS(soli->hora_entrada * 60), nroArea);
	}
}

void escribirAreas(FILE *file, AreaCola *areasCola, TipoSolicitud *tiposSolicitud, int cantTipoSoli, int* cantTotalPendientes, int* cantTotalProcesadas)
{
	fprintf(file, "\nResumen del estado de las áreas:\n");
	for (int i = 0; i < 5; i++)
	{
		fprintf(file, "\nÁrea %d\n", i + 1);
		AreaCola area = areasCola[i];
		fprintf(file, "%-23s%-25s%-25s\n", "Tipo de Solicitud", "Cantidad Procesadas", "Cantidad Pendientes"); //se crea el titulo
		for (int j = 0; j < cantTipoSoli; j++)
		{
			fprintf(file, "%-32s%-25d%-25d\n", tiposSolicitud[j].nom_soli, area.cantSoliProcesadasPorTipo[j], area.cantSoliPorTipo[j]);
		}
		for(int i=0; i<67; ++i) fprintf(file,"-");
		fprintf(file,"\n%-32s%-25d%d\n","Total:", areasCola[i].cantTotalSoliProc, areasCola[i].cantTotalSoli);
		*(cantTotalPendientes) += area.cantTotalSoli;
		*(cantTotalProcesadas) += area.cantTotalSoliProc;
	}
}
