#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <time.h>

/*
 * miu: el promedio a utilizar en la distribucion de Poisson
 * t:  El tiempo en que k solicitudes de cierto tipo llegan
 * maxCant: La máxima cantidad de solicitudes que pueden llegar segun el tipo
*/
int getCantSoli(const gsl_rng *r, double miu, double t, int maxCant);
double getTiempoEntreSoli(const gsl_rng *r, double miuPoisson, double tiempoActual, double maxTiempo);

int getCantSoli(const gsl_rng *r, double miuPoisson, double t, int maxCant)
{
	int k;
	do
	{
		k = gsl_ran_poisson(r, miuPoisson * t);
	} while (k > maxCant);

	return k;
}

double getTiempoEntreSoli(const gsl_rng *r, double miuPoisson, double tiempoActual, double maxTiempo)
{
	double tiempo;
	double miuExpo = 1 / miuPoisson;
	do
	{
		tiempo = gsl_ran_exponential(r, miuExpo);
	} while (tiempo + tiempoActual > maxTiempo);

	return tiempo;
}

double getTiempoProcesamiento(const gsl_rng *r, double desviacion, double promedio)
{
	double x = gsl_ran_gaussian(r, desviacion);
	x += promedio;
	return x;
}
