#include "parametrogeneral.h"

int main(int argc, char const *argv[])
{
	int cantTipoSoli = 25;
	int opt = 1, opcion, i = 0;
	TipoSolicitud arraytipoSoli[] = 
	{	{1, "Soli 1", 0.04}, 		{2, "Soli 2", 0.04}, 		{3, "Soli 3", 0.04},		{4, "Soli 4", 0.04},		{5, "Soli 5", 0.04},
		{6, "Soli 6", 0.04},		{7, "Soli 7", 0.04},		{8, "Soli 8", 0.04},		{9, "Soli 9", 0.04},		{10, "Soli 10", 0.04},
		{11, "Soli 11", 0.04}, 	{12, "Soli 12", 0.04},	{13, "Soli 13", 0.04},	{14, "Soli 14", 0.04},	{15, "Soli 15", 0.04},
		{16, "Soli 16", 0.04}, 	{17, "Soli 17", 0.04},	{18, "Soli 18", 0.04},	{19, "Soli 19", 0.04},	{20, "Soli 20", 0.04},
		{21, "Soli 21", 0.04}, 	{22, "Soli 22", 0.04},	{23, "Soli 23", 0.04},	{24, "Soli 24", 0.04},	{25, "Soli 25", 0.04},
	};
	escribirTipoDeSolicitudes(arraytipoSoli, cantTipoSoli);
	leerTipoDeSolicitudes(cantTipoSoli);

	//Temporal
	Tramite *tramites = (Tramite *)calloc(sizeof(Tramite), cantTipoSoli);

	tramites[0].id_tipo_soli = 1;
	tramites[0].areas[0].nro_area = 3;
	tramites[0].areas[0].tiempo_prom = 11;
	tramites[0].areas[0].desv_tipica = 0.05;

	tramites[0].areas[1].nro_area = 1;
	tramites[0].areas[1].tiempo_prom = 11;
	tramites[0].areas[1].desv_tipica = 0.03;

	tramites[0].areas[2].nro_area = 4;
	tramites[0].areas[2].tiempo_prom = 11;
	tramites[0].areas[2].desv_tipica = 0.01;

	tramites[1].id_tipo_soli = 2;
	tramites[1].areas[0].nro_area = 5;
	tramites[1].areas[0].tiempo_prom = 11;
	tramites[1].areas[0].desv_tipica = 0.15;

	tramites[1].areas[1].nro_area = 4;
	tramites[1].areas[1].tiempo_prom = 11;
	tramites[1].areas[1].desv_tipica = 0.35;

	tramites[1].areas[2].nro_area = 2;
	tramites[1].areas[2].tiempo_prom = 11;
	tramites[1].areas[2].desv_tipica = 0.71;

	tramites[2].id_tipo_soli = 3;
	tramites[2].areas[0].nro_area = 1;
	tramites[2].areas[0].tiempo_prom = 11;
	tramites[2].areas[0].desv_tipica = 0.12;

	tramites[2].areas[1].nro_area = 2;
	tramites[2].areas[1].tiempo_prom = 11;
	tramites[2].areas[1].desv_tipica = 0.35;

	tramites[2].areas[2].nro_area = 3;
	tramites[2].areas[2].tiempo_prom = 11;
	tramites[2].areas[2].desv_tipica = 0.05;

	tramites[2].areas[3].nro_area = 4;
	tramites[2].areas[3].tiempo_prom = 11;
	tramites[2].areas[3].desv_tipica = 0.71;

	tramites[3].id_tipo_soli = 4;
	tramites[3].areas[0].nro_area = 1;
	tramites[3].areas[0].tiempo_prom = 11;
	tramites[3].areas[0].desv_tipica = 0.12;

	tramites[4].id_tipo_soli = 5;
	tramites[4].areas[0].nro_area = 1;
	tramites[4].areas[0].tiempo_prom = 11;
	tramites[4].areas[0].desv_tipica = 0.12;

	tramites[5].id_tipo_soli = 6;
	tramites[5].areas[0].nro_area = 1;
	tramites[5].areas[0].tiempo_prom = 11;
	tramites[5].areas[0].desv_tipica = 0.12;

	tramites[6].id_tipo_soli = 7;
	tramites[6].areas[0].nro_area = 1;
	tramites[6].areas[0].tiempo_prom = 11;
	tramites[6].areas[0].desv_tipica = 0.12;

	tramites[7].id_tipo_soli = 8;
	tramites[7].areas[0].nro_area = 1;
	tramites[7].areas[0].tiempo_prom = 11;
	tramites[7].areas[0].desv_tipica = 0.12;

	tramites[8].id_tipo_soli = 9;
	tramites[8].areas[0].nro_area = 1;
	tramites[8].areas[0].tiempo_prom = 11;
	tramites[8].areas[0].desv_tipica = 0.12;

	tramites[9].id_tipo_soli = 10;
	tramites[9].areas[0].nro_area = 1;
	tramites[9].areas[0].tiempo_prom = 11;
	tramites[9].areas[0].desv_tipica = 0.12;

	tramites[10].id_tipo_soli = 11;
	tramites[10].areas[0].nro_area = 1;
	tramites[10].areas[0].tiempo_prom = 11;
	tramites[10].areas[0].desv_tipica = 0.12;

	tramites[11].id_tipo_soli = 12;
	tramites[11].areas[0].nro_area = 1;
	tramites[11].areas[0].tiempo_prom = 11;
	tramites[11].areas[0].desv_tipica = 0.12;

	tramites[12].id_tipo_soli = 13;
	tramites[12].areas[0].nro_area = 1;
	tramites[12].areas[0].tiempo_prom = 11;
	tramites[12].areas[0].desv_tipica = 0.12;

	tramites[13].id_tipo_soli = 14;
	tramites[13].areas[0].nro_area = 1;
	tramites[13].areas[0].tiempo_prom = 11;
	tramites[13].areas[0].desv_tipica = 0.12;

	tramites[14].id_tipo_soli = 15;
	tramites[14].areas[0].nro_area = 1;
	tramites[14].areas[0].tiempo_prom = 11;
	tramites[14].areas[0].desv_tipica = 0.12;

	tramites[15].id_tipo_soli = 16;
	tramites[15].areas[0].nro_area = 1;
	tramites[15].areas[0].tiempo_prom = 11;
	tramites[15].areas[0].desv_tipica = 0.12;

	tramites[16].id_tipo_soli = 17;
	tramites[16].areas[0].nro_area = 1;
	tramites[16].areas[0].tiempo_prom = 11;
	tramites[16].areas[0].desv_tipica = 0.12;

	tramites[17].id_tipo_soli = 18;
	tramites[17].areas[0].nro_area = 1;
	tramites[17].areas[0].tiempo_prom = 11;
	tramites[17].areas[0].desv_tipica = 0.12;

	tramites[18].id_tipo_soli = 19;
	tramites[18].areas[0].nro_area = 1;
	tramites[18].areas[0].tiempo_prom = 11;
	tramites[18].areas[0].desv_tipica = 0.12;

	tramites[19].id_tipo_soli = 20;
	tramites[19].areas[0].nro_area = 1;
	tramites[19].areas[0].tiempo_prom = 11;
	tramites[19].areas[0].desv_tipica = 0.12;

	tramites[20].id_tipo_soli = 21;
	tramites[20].areas[0].nro_area = 1;
	tramites[20].areas[0].tiempo_prom = 11;
	tramites[20].areas[0].desv_tipica = 0.12;

	tramites[21].id_tipo_soli = 22;
	tramites[21].areas[0].nro_area = 1;
	tramites[21].areas[0].tiempo_prom = 11;
	tramites[21].areas[0].desv_tipica = 0.12;

	tramites[22].id_tipo_soli = 23;
	tramites[22].areas[0].nro_area = 1;
	tramites[22].areas[0].tiempo_prom = 11;
	tramites[22].areas[0].desv_tipica = 0.12;

	tramites[23].id_tipo_soli = 24;
	tramites[23].areas[0].nro_area = 1;
	tramites[23].areas[0].tiempo_prom = 11;
	tramites[23].areas[0].desv_tipica = 0.12;

	tramites[24].id_tipo_soli = 25;
	tramites[24].areas[0].nro_area = 1;
	tramites[24].areas[0].tiempo_prom = 11;
	tramites[24].areas[0].desv_tipica = 0.12;

	escribirTramites(tramites, cantTipoSoli);
	leerTramites(cantTipoSoli);
	/*while(opt)
	{
		printf("0- Salir\n1- Agregar\n2- Borrar\n3- Modificar\n");
		scanf("%d",&opt);
		i=0;
		if(opt==1)
		{
			printf("Solicitudes actuales: %d\nId: ", cantTipoSoli);
			scanf("%d", &opcion);
			if(opcion>0)
			{	//hay que utilizar realloc para redimensionar el vector cuando se quiera ingresar mas campos
				cantTipoSoli++;		//se agrega un nuevo campo
				tramites = (Tramite *)realloc(tramites, (cantTipoSoli)*sizeof(Tramite));
				memset(&tramites[opcion-1], 0, sizeof(Tramite));		//inicializa a 0 el nuevo espacio
				tramites[opcion-1].id_tipo_soli=opcion;
				while(i<5)
				{
					printf("Ingrese area, tiempo promedio y desviacion tipica, area=0 terminar\n");
					scanf("%d", &tramites[opcion-1].areas[i].nro_area);
					if(tramites[opcion-1].areas[i].nro_area!=0)
					{	//lf para double
						scanf("%lf%lf", &tramites[opcion-1].areas[i].tiempo_prom, &tramites[opcion-1].areas[i].desv_tipica);
					}
					else
						i=5;
					i++;
				}
			}
			else
				printf("Id invalido\n");
			opcion=1;
		}
		else if(opt==2)
			eliminarTramites(tramites,cantTipoSoli);
		else if(opt==3)
			editarTramites(tramites,cantTipoSoli);
		escribirTramites(tramites,cantTipoSoli);		//escribe todo en el archivo, se deberia de tener una funcion actualizar para cada caso
		leerTramites(cantTipoSoli);
	}*/
	return 0;
}
