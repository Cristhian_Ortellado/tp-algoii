#include <stdio.h>
#include "entidades.h"
#include <stdlib.h>
#include <string.h> //para memset
#include "utilidades.h"

/*
 * Funcion para leer los tramites de cada solicitud
 * 
**/
Tramite *leerTramites(int cantTipoSoli);

/* Funcion: eliminarTipoSolicitud
 * Actualiza el archivo de tramites con los datos datos
 * 
 * -tramites: Array de Tramite con los datos actualizados
 * 
 * -cantTipoSoli : Cantidad de tipo de Solicitudes que contienen un trámite
*/
void escribirTramites(Tramite tramites[], int cantTipoSoli);

/* Funcion: eliminarTipoSolicitud
 * ------------------------------
 * Elimina el tipo de solicitud y toda su informacion de los dos vectores, y redimensiona los vectores dados
 * 
 * -tiposDeSolicitud: puntero al array de tipo de Solicitudes
 * 
 * -tramites:  puntero al array de tramites
 * 
 * -id: Id del tipo de Solicitud a eliminar
 * 
 * -cantTipoSoli: Cantidad actual de tipo de Solicitudes (antes de eliminar
 * 				 el tipo de Solicitud con id proveido)
*/
void eliminarTipoSolicitud(TipoSolicitud **tiposDeSolicitud, Tramite **tramites, int id, int *cantTipoSoli);

/* Funcion: obtenerCantTipoDeSolicitudes
 * Obtiene la cantidad de tipo de solicitudes guardados en el archivo txt correspondiente
 * 
 * retorna: el número de tipo de solicitudes

*/
int obtenerCantTipoDeSolicitudes();

void escribirUltimoPar(ParametrosEspecificos params);
int leerUltimoPar(char *codigoSim, char *duracionSim, char *cantSoli, char *intervaloAct);

void escribirUltimoPar(ParametrosEspecificos params)
{
	FILE *ultSim = fopen(FILE_TIPO_ULTSIMULACION, "wb");
	fwrite(&params, sizeof(ParametrosEspecificos), 1, ultSim);
	fclose(ultSim);
}

int leerUltimoPar(char *codigoSim, char *duracionSim, char *cantSoli, char *intervaloAct)
{
	FILE *ultSim = fopen(FILE_TIPO_ULTSIMULACION, "rb");
	ParametrosEspecificos aux;
	char temp[6];
	if (!ultSim)
		return 0;
	fread(&aux, sizeof(ParametrosEspecificos), 1, ultSim);

	//cadenas de igual tamaño
	strcpy(codigoSim, aux.cod_sim);

	//se tiene en cuenta cuantos decimales se va copiar
	if (aux.dur_sim > 0 && aux.dur_sim < 100)
		sprintf(temp, "%.2f", aux.dur_sim);
	else if (aux.dur_sim >= 100 && aux.dur_sim < 1000)
		sprintf(temp, "%.1f", aux.dur_sim);
	else
		sprintf(temp, "%.0f", aux.dur_sim);
	strcpy(duracionSim, temp);

	//cant_soli es entero, no hace falta verificar decimales
	sprintf(temp, "%d", aux.cant_soli);
	strcpy(cantSoli, temp);

	//verificar decimales
	if (aux.inter_act > 0 && aux.inter_act < 100)
		sprintf(temp, "%.2f", aux.inter_act);
	else if (aux.inter_act >= 100 && aux.inter_act < 1000)
		sprintf(temp, "%.1f", aux.inter_act);
	else
		sprintf(temp, "%.0f", aux.inter_act);
	strcpy(intervaloAct, temp);

	fclose(ultSim);
	return 1;
}

Tramite *leerTramites(int cantTipoSoli)
{
	int i, j;
	FILE *file = fopen(FILE_TRAMITE, "rb");

	Tramite *tramites = (Tramite *)calloc(sizeof(Tramite), cantTipoSoli);

	fread(tramites, sizeof(Tramite), cantTipoSoli, file);
	// for (i = 0; i < cantTipoSoli; i++)
	// {
	// 	if (tramites[i].id_tipo_soli != 0)
	// 	{
	// 		printf("Id: %d\n", tramites[i].id_tipo_soli);
	// 		for (j = 0; tramites[i].areas[j].nro_area != 0 && j < 5; j++)
	// 		{
	// 			printf("Nro Area: %d,   Tiempo Promedio: %.2f    Desviacion tipica: %.2f \n", tramites[i].areas[j].nro_area, tramites[i].areas[j].tiempo_prom, tramites[i].areas[j].desv_tipica);
	// 		}
	// 		printf("\n");
	// 	}
	// }
	return tramites;
}

TipoSolicitud *leerTipoDeSolicitudes(int cantTipoSoli)
{
	int i;
	FILE *file = fopen(FILE_TIPO_SOLICITUDES, "rb");

	TipoSolicitud *tiposDeSolicitud = (TipoSolicitud *)calloc(sizeof(TipoSolicitud), cantTipoSoli);

	fread(tiposDeSolicitud, sizeof(TipoSolicitud), cantTipoSoli, file);
	// for (i = 0; i < cantTipoSoli; i++)
	// {
	// 	printf("Nombre Solicitud: %s,   Probabilidad: %.2f\n", tiposDeSolicitud[i].nom_soli, tiposDeSolicitud[i].prob_soli);
	// }
	fclose(file);
	return tiposDeSolicitud;
}

void escribirTipoDeSolicitudes(TipoSolicitud tiposDeSolicitud[], int cantTipoSoli)
{
	FILE *file = fopen(FILE_TIPO_SOLICITUDES, "wb+");
	fwrite(tiposDeSolicitud, sizeof(TipoSolicitud), cantTipoSoli, file);
	fclose(file);
}
int obtenerCantTipoDeSolicitudes()
{
	FILE *file = fopen(FILE_TIPO_SOLICITUDES, "rb");
	if (!file)
		return 0;
	fseek(file, 0, SEEK_END);
	int cant = ftell(file) / sizeof(TipoSolicitud);
	fclose(file);
	return cant;
}

void escribirTramites(Tramite tramites[], int cantTipoSoli)
{
	FILE *file = fopen(FILE_TRAMITE, "wb+");
	int i, k = 1;
	fwrite(tramites, sizeof(Tramite), cantTipoSoli, file);
	fclose(file);
}

void eliminarTipoSolicitud(TipoSolicitud **tiposDeSolicitud, Tramite **tramites, int id, int *cantTipoSoli)
{
	TipoSolicitud *viejoS = *tiposDeSolicitud;
	Tramite *viejoT = *tramites;
	*tiposDeSolicitud = malloc(sizeof(TipoSolicitud) * (*cantTipoSoli - 1));
	*tramites = malloc(sizeof(Tramite) * (*cantTipoSoli - 1));
	int c = 0;
	for (int i = 0; i < *cantTipoSoli; i++)
	{
		if (i != id - 1)
		{
			(*tiposDeSolicitud)[c] = viejoS[i];
			(*tiposDeSolicitud)[c].id_tipo_soli = c + 1;
			(*tramites)[c] = viejoT[i];
			(*tramites)[c].id_tipo_soli = c + 1;
			++c;
		}
	}
	free(viejoS);
	free(viejoT);
	(*cantTipoSoli)--;
}

void ajustarProbabilidad(TipoSolicitud *tiposDeSolicitud, int cantTipoSoli, double valor, double total, char *tipoSolicitudesModif)
{
	if (total == 0)
	{
		for (int i = 0; i < cantTipoSoli; ++i)
		{
			if (tipoSolicitudesModif[i] == '0')
				total += tiposDeSolicitud[i].prob_soli;
		}
	}
	for (int i = 0; i < cantTipoSoli; ++i)
	{
		if (tipoSolicitudesModif[i] == '0')
			tiposDeSolicitud[i].prob_soli += tiposDeSolicitud[i].prob_soli * valor / total;
	}
}
